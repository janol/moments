package algorithm;

/***
 * Class to extend
 * Implements get and setSolution; this will be returned to Solver
 * @author OLJA100
 *
 */
public abstract class EternityAlgorithm extends Algorithm {
	private static final long serialVersionUID = 2954169205297927059L;
	
	@Override
	public abstract boolean calculateStep();
}
