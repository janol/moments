package algorithm;

import basics.*;

/**
 * Simple Algorithm for 2x2 to test if rest Works
 * @author OLJA100
 *
 */
public class TestAlgorithmObject extends EternityAlgorithm {
	private static final long serialVersionUID = 8349792434790709067L;
	
	private int depth = 0;
	private Tile[] tiles;
	private Board board;
	private boolean solved;
	
	public TestAlgorithmObject(Board board, Tile[] tiles, Hint[] hints){
		int dimX = board.getDimensionX();
		int dimY = board.getDimensionY();
		this.tiles = tiles;
		this.board = new EternityBoard(dimX, dimY);
		this.solved = false;
	}
	
	@Override
	public boolean calculateStep() {
		try{
			switch (this.depth){
				case 0: this.board.placeTile(0, 0, 2, tiles[0]);
						depth++;
						break;
				case 1: this.board.placeTile(0, 1, 3, tiles[1]);
						depth++;
						break;
				case 2: this.board.placeTile(1, 0, 0, tiles[2]);
						depth++;
						break;
				case 3: this.board.placeTile(1, 1, 1, tiles[3]);
						depth++;
						this.solved = true;
						break;
			}
		} catch (Exception e){	
		}
		
		return this.depth>=0;
	}

	@Override
	public boolean isSolved() {
		return this.solved;
	}

	@Override
	public int getDepth() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Board getBoard() {
		return this.board;
	}

}
