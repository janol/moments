package algorithm;

import java.util.ArrayList;
import java.util.HashMap;

import basics.Board;
import basics.CloneObject;
import basics.LinearRegression;

public class EternityAlgorithmState extends AlgorithmState {
	private static final long serialVersionUID = 2892403758135509806L;

	private final long TIME_BORDER = 10;
	
	private int highscore;
	private int recentHighscore;
	private int recentBacktrack;
	private long stepCount;
	private long startTime;
	private Algorithm algo = null;
	private int saveBorder;
	private Board highscoreBoard = null;
	private HashMap<Integer, Board> highscoreMap = null;
	private ArrayList<Long> highscoreTimes;
	
	public EternityAlgorithmState() {
		this.highscore = 0;
		this.recentHighscore = 0;
		this.recentBacktrack = Integer.MAX_VALUE;
		this.stepCount = 0;
		this.startTime = 0;
		this.highscoreMap = new HashMap<Integer, Board>();
		this.highscoreTimes = new ArrayList<Long>();
	}
	
	public EternityAlgorithmState(Algorithm algo) {
		this.highscore = 0;
		this.recentHighscore = 0;
		this.recentBacktrack = Integer.MAX_VALUE;
		this.stepCount = 0;
		this.startTime = 0;
		this.algo = algo;
		this.highscoreMap = new HashMap<Integer, Board>();
		this.saveBorder = algo.getBoard().getDimensionX() 
			* algo.getBoard().getDimensionY() / 2;
		this.highscoreTimes = new ArrayList<Long>();
	}

	@Override
	public void checkDepth(int depth) {
		
		//check whether depth changes state of algorithm
		if (depth > this.highscore) {
			this.highscore = depth;
			this.recentHighscore = depth;
			
			if ((this.algo != null) && (this.highscore > this.saveBorder)) {
				
				long time = System.currentTimeMillis() - this.startTime;
				
				//check for values < 10 ms.. 0 would be an exception and everything else destroys the calculation
				if (time > this.TIME_BORDER) {
					this.highscoreTimes.add(time);
				}
				
				//create copy of highscore
				try {
					this.highscoreBoard = (Board) CloneObject.deepCopy(this.algo.getBoard());
					this.highscoreMap.put(this.highscore, this.highscoreBoard);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else if (depth > this.recentHighscore) {
			this.recentHighscore = depth;
		}
		
		if (depth < this.recentBacktrack){
			this.recentBacktrack = depth;
		}
		
		this.stepCount++;
	}

	@Override
	public long getCalculationTime() {
		return System.currentTimeMillis()-this.startTime;
	}

	@Override
	public int getHighscore() {
		return this.highscore;
	}

	@Override
	public int getRecentBacktrack() {
		int tmp = this.recentBacktrack;
		this.recentBacktrack = Integer.MAX_VALUE;
		return tmp;
	}

	@Override
	public int getRecentHighscore() {
		int tmp = this.recentHighscore;
		this.recentHighscore = Integer.MIN_VALUE;
		return tmp;
	}

	@Override
	public long getStepCount() {
		return this.stepCount;
	}

	@Override
	public void startCalculation() {
		this.startTime = System.currentTimeMillis();
	}

	@Override
	public Board getHighscoreBoard() {
		return this.highscoreBoard;
	}

	@Override
	public HashMap<Integer, Board> getHighscoreBoardMap() {
		return this.highscoreMap;
	}

	@Override
	public long getTimeToNextHighscore() {
		//TODO have to discuss whether actual time should be added or not
		
		long time = System.currentTimeMillis() - this.startTime;
		
		this.highscoreTimes.add(time);
		
		time = LinearRegression.getNextTime(this.highscoreTimes);
		
		this.highscoreTimes.remove(this.highscoreTimes.size() - 1);
		
		return time;
	}

}
