package algorithm.test;

import static org.junit.Assert.*;

import io.EternityTileReader;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.junit.*;

import exceptions.EmptyFileException;
import exceptions.NoFileException;
import exceptions.PlacingException;

import algorithm.*;
import basics.*;

public class AlgorithmStateTest {
	private AlgorithmState state;
	
	@Before
	public void setUp() {
		state = new EternityAlgorithmState();
		state.checkDepth(200);
	}


	@Test
	public void testGetRecentHighscoreLower() {
		state.getRecentHighscore();
		state.checkDepth(150);
		assertEquals(150, state.getRecentHighscore());
		assertEquals(200, state.getHighscore());
	}
	
	@Test
	public void testGetRecentHighscoreHigher() {
		state.getRecentHighscore();
		state.checkDepth(220);
		assertEquals(220, state.getRecentHighscore());
		assertEquals(220, state.getHighscore());
	}

	@Test
	public void testGetHighscore() {
		assertEquals(200, state.getHighscore());
		state.checkDepth(230);
		assertEquals(230, state.getHighscore());
	}

	@Test
	public void testGetRecentBacktrack() {
		state.checkDepth(10);
		assertEquals(10, state.getRecentBacktrack());
		assertEquals(200, state.getHighscore());
		
		state.checkDepth(180);
		assertEquals(180, state.getRecentBacktrack());
		assertEquals(200, state.getHighscore());
		
	}

	@Test
	public void testGetStepCount() {
		assertEquals(1, state.getStepCount());
		
		for (int i = 0; i < 10; i++) {
			state.checkDepth(i+100);
		}
	
		assertEquals(11, state.getStepCount());
	}

	/*
	 * Don't know how to test time yet..
	 */
	@Test
	public void testGetCalculationTime() throws InterruptedException {
		state.startCalculation();
		Thread.sleep(10);
		assertEquals(10., state.getCalculationTime(), 1);
	}
	
	@Test
	public void testGetHighScoreBoard() throws IOException, EmptyFileException, NoFileException {
		EternityTileReader TReader = new EternityTileReader();
		Tile[] tiles = TReader.getTiles("resources" + File.separator + "tiles2x2.txt");
		Algorithm eternityAlgorithm = new BacktrackAlgorithm(new EternityBoard(2,2),tiles,new EternityHint[0]);
		state = new EternityAlgorithmState(eternityAlgorithm);
		//do step one
		for (int i = 0; i < 4; i++){
			eternityAlgorithm.calculateStep();
			state.checkDepth(eternityAlgorithm.getDepth());
		}
		
		assertTrue(TestSolution.isSolution(state.getHighscoreBoard()));
	}
	
	@Test
	public void testGetHighScoreBoardMap() throws IOException, EmptyFileException, NoFileException, PlacingException {
		EternityTileReader TReader = new EternityTileReader();
		Tile[] tiles = TReader.getTiles("resources" + File.separator + "tiles2x2.txt");
		Algorithm eternityAlgorithm = new BacktrackAlgorithm(new EternityBoard(2,2),tiles,new EternityHint[0]);
		state = new EternityAlgorithmState(eternityAlgorithm);
		//do step one
		for (int i = 0; i < 4; i++){
			eternityAlgorithm.calculateStep();
			state.checkDepth(eternityAlgorithm.getDepth());
		}
		
		HashMap<Integer, Board> highscoresGiven = new HashMap<Integer, Board>();
		HashMap<Integer, Board> highscoresAlgorithm = state.getHighscoreBoardMap();
		
		Board b = new EternityBoard(2, 2);
		b.placeTile(0, 0, 3, tiles[0]);
		b.placeTile(0, 1, 2, tiles[3]);
		b.placeTile(1, 0, 0, tiles[1]);
		highscoresGiven.put(3, b);
		
		Board b1 = new EternityBoard(2, 2);
		b1.placeTile(0, 0, 3, tiles[0]);
		b1.placeTile(0, 1, 2, tiles[3]);
		b1.placeTile(1, 0, 0, tiles[1]);
		b1.placeTile(1, 1, 1, tiles[2]);
		highscoresGiven.put(4, b1);
		
		
		for (Integer key: highscoresGiven.keySet()) {
			if (!highscoresGiven.get(key).equals(highscoresAlgorithm.get(key))) {
				fail("Board are not the same");
			}
		}
	}
}
