package algorithm.test;

import static org.junit.Assert.*;

import java.io.*;

import org.apache.log4j.Logger;
import org.junit.*;

import basics.Log4jConfigurator;
import basics.TestSolution;

import exceptions.AlgorithmInstantiationException;
import exceptions.PlacingException;

import main.*;

public class SimpleBackTrackerTest {
	private static final Logger LOGGER =
		Logger.getLogger(SimpleBackTrackerTest.class);
	
	@Before
	public void setUp() {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
	}
		
	@Test
	public void test2x2SingleThreaded() throws PlacingException,
			AlgorithmInstantiationException {
		SingleThreadedEternity sTE = new SingleThreadedEternity(
				"resources" + File.separator + "tiles2x2.txt",
				"resources" + File.separator + "hints2x2.txt", 2,
				2, "algorithm.SimpleBacktracker");
		sTE.calculate();

		assertTrue(TestSolution.isSolution(sTE.getSolution()));
	}
	
	@Test
	public void test4x4SingleThreaded() throws PlacingException,
			AlgorithmInstantiationException {
		SingleThreadedEternity sTE = new SingleThreadedEternity(
				"resources" + File.separator + "tiles4x4.txt",
				"resources" + File.separator + "hints4x4.txt", 4,
				4, "algorithm.SimpleBacktracker");
		sTE.calculate();

		assertTrue(TestSolution.isSolution(sTE.getSolution()));
		
		LOGGER.debug(sTE.getSolution());
	}
	
	@Test
	public void test4x4with1HintSingleThreaded() throws PlacingException,
			AlgorithmInstantiationException {
		SingleThreadedEternity sTE = new SingleThreadedEternity(
				"resources" + File.separator + "tiles4x4.txt",
				"resources" + File.separator + "hints4x4_1tile.txt", 4,
				4, "algorithm.SimpleBacktracker");
		sTE.calculate();

		assertTrue(TestSolution.isSolution(sTE.getSolution()));
		
		LOGGER.debug(sTE.getSolution());
	}
	
	@Test
	public void testFailOn4x4SingleThreaded() throws PlacingException,
			AlgorithmInstantiationException {
		SingleThreadedEternity sTE = new SingleThreadedEternity(
				"resources" + File.separator + "tilesFail4x4.txt",
				"resources" + File.separator + "hintsFail4x4.txt", 4, 4,
				"algorithm.SimpleBacktracker");
		sTE.calculate();

		assertFalse(TestSolution.isSolution(sTE.getSolution()));
	}
		
	@Test
	public void testFailOn4x4MultiThreaded() throws PlacingException,
			AlgorithmInstantiationException {
		MultiThreadedEternityTestVersion mTE =
			new MultiThreadedEternityTestVersion(
					"resources" + File.separator + "tilesFail4x4.txt",
					"resources" + File.separator + "hintsFail4x4.txt",
					4, 4, "algorithm.SimpleBacktracker");
		mTE.calculate();

		assertFalse(TestSolution.isSolution(mTE.getSolution()));
	}
	
	
	@Test
	public void test4x4MultiThreaded() throws PlacingException,
			AlgorithmInstantiationException {
		MultiThreadedEternityTestVersion mTE =
			new MultiThreadedEternityTestVersion(
					"resources" + File.separator + "tiles4x4.txt",
					"resources" + File.separator + "hints4x4.txt",
					4, 4, "algorithm.SimpleBacktracker");
		mTE.calculate();
		
		assertTrue(TestSolution.isSolution(mTE.getSolution()));
	}
	
	@Test
//	@Ignore
	public void test10x10MultiThreaded() throws PlacingException,
			AlgorithmInstantiationException{
//		File logFile = new File("C:\\temp\\log10x10.txt");
//		if(logFile.exists()) {
//			logFile.delete();
//		}
//		Logger.getRootLogger().addAppender(new FileAppender(new PatternLayout(
//				"%d{dd MMM yyy HH:mm:ss} [%t] %-5p %c %x - %m%n"), logFile.getCanonicalPath()));
		MultiThreadedEternityTestVersion mTE =
			new MultiThreadedEternityTestVersion(
					"resources" + File.separator + "tiles10x10.txt",
					"resources" + File.separator + "hints10x10.txt",
					10, 10, "algorithm.SimpleBacktracker");
		mTE.calculate();
		
		assertTrue(TestSolution.isSolution(mTE.getSolution()));
	}

	@Ignore
	@Test
	public void test10x11MultiThreaded() throws PlacingException,
			AlgorithmInstantiationException{
		MultiThreadedEternityTestVersion mTE =
			new MultiThreadedEternityTestVersion(
					"resources" + File.separator + "E_10_11.b13i13.txt",
					"resources" + File.separator + "emptyHints.txt",
					10, 10, "algorithm.SimpleBacktracker");
		mTE.calculate();
		
		assertTrue(TestSolution.isSolution(mTE.getSolution()));
	}	

	@Ignore
	@Test
	public void test10x11SingleThreaded() throws PlacingException,
			AlgorithmInstantiationException{
		SingleThreadedEternity sTE = new SingleThreadedEternity(
				"resources" + File.separator + "E_10_11.b13i13.txt",
				"resources" + File.separator + "emptyHints.txt",
				10, 10, "algorithm.SimpleBacktracker");
		sTE.calculate();
		
		assertTrue(TestSolution.isSolution(sTE.getSolution()));
	}	
}
