package algorithm.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import io.EternityTileReader;

import org.junit.Before;
import org.junit.Test;

import exceptions.EmptyFileException;
import exceptions.NoFileException;

import algorithm.BacktrackAlgorithm;
import basics.EternityBoard;
import basics.EternityHint;
import basics.Log4jConfigurator;
import basics.Tile;

public class BacktrackAlgorithmTest {
	
	private BacktrackAlgorithm algo;
	private EternityTileReader reader;
	
	@Before
	public void setUp() throws IOException, EmptyFileException, NoFileException {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
		this.reader = new EternityTileReader();
		Tile[] tiles = null;
		tiles = reader.getTiles("resources" + File.separator + "tiles2x2.txt");
		this.algo = new BacktrackAlgorithm(new EternityBoard(2,2), tiles, new EternityHint[0]);
	}
	
	@Test
	public void testIsSolved() {
		for (int i = 0; i < 4; i++) {
			this.algo.calculateStep();
		}
		
		assertTrue(this.algo.isSolved());
	}
	
	@Test
	public void testIsNotSolved() {
		//not enough steps to be solved
		for (int i = 0; i < 3; i++) {
			this.algo.calculateStep();
		}
		
		assertFalse(this.algo.isSolved());
	}

	@Test
	public void testGetDepthof0() {
		assertEquals(0, this.algo.getDepth());
	}
	
	@Test
	public void testGetDepthof3() {
		for (int i = 0; i < 3; i++) {
			this.algo.calculateStep();
		}
		
		assertEquals(3, this.algo.getDepth());
	}

	@Test
	public void testGetBoard() {
		fail("Not yet implemented");
	}

}
