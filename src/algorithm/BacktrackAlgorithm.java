package algorithm;

import java.util.*;

import exceptions.*;

import basics.*;

import basics.Tile.Side;

/**
 * Implements a simple Backtracking Algorithm
 * @author OLJA100
 *
 */
public class BacktrackAlgorithm extends Algorithm {
	private static final long serialVersionUID = -9119095486461458715L;
	

	private Board board;
	private Tile[] tiles = null;
	private int depth;
	private boolean[] used;
	private boolean solved;

	public BacktrackAlgorithm(Board board, Tile[] tiles, Hint[] hints) {
		
		//get Board Dimensions
		int dimX = board.getDimensionX();
		int dimY = board.getDimensionY();

		this.board = new EternityBoard(dimX, dimY);
		this.tiles = tiles;
		this.depth = 0;
		this.used = new boolean[this.tiles.length];
		Arrays.fill(this.used, false);
		this.solved = false;
		
		//place Hints
		for (int i = 0; i < hints.length; i++ ){
			int[] pos = new int[]{hints[i].getPosX(), hints[i].getPosY()};
			int id = hints[i].getID()-1;
			int rot = hints[i].getRotation();
			
			try {
				this.board.placeTile(pos[0], pos[1], rot, this.tiles[id]);
				this.board.setKnown(pos[0], pos[1], true);
				this.used[id]=true;
			} catch (PlacingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/*
	 * returns the rotation of a fitting tile if tile does not fit returns -1
	 */
	@SuppressWarnings("deprecation")
	private int check(int posX, int posY, int tile, int rotation) {
		int rotated = -1;
		boolean exit = false;

		int[] pos = nextPos();
		
		// get surroingColors
		int[] sColors = board.getSurroundingColors(pos[0], pos[1]);

		// check all 4 rotations if the sides fit
		for (int i = rotation; (i < 4) && (!exit); i++) {
			if (((sColors[Side.BOTTOM.get()] == -1) 
					|| (sColors[Side.BOTTOM.get()] == this.tiles[tile].getRotatedColor(Side.BOTTOM, i)))
				&& ((sColors[Side.TOP.get()] == -1) 
					|| (sColors[Side.TOP.get()] == this.tiles[tile].getRotatedColor(Side.TOP, i)))
				&& ((sColors[Side.RIGHT.get()] == -1) 
					|| (sColors[Side.RIGHT.get()] == this.tiles[tile].getRotatedColor(Side.RIGHT, i)))
				&& ((sColors[Side.LEFT.get()] == -1) 
					|| (sColors[Side.LEFT.get()] == this.tiles[tile].getRotatedColor(Side.LEFT, i)))) {
				rotated = i;
				exit = true;
			}
		}

		return rotated;
	}

	//calculates the Board one step at a time
	@Override
	public boolean calculateStep(){
		boolean exit = false;
		int startTile = 0;
		int startRotation = 0;
		int useRotation = 0;
		int[] pos;
		int[] newPos;
		
		//dirty hack to clean somehow the mistake in the stateresourcfile
		if (this.depth<0) {
			this.depth = 0;
		}
		
		// get next Position
		do {
			pos = nextPos();
			if (this.board.getCell(pos[0], pos[1]).isKnown()){
				depth++;
				if ((this.depth >= this.tiles.length)){
					exit=true;
				}
			} else{
				exit=true;
			}
		} while (!exit);
		
		
		//if depth >= tiles length there is a solution and the last tile is known
		if (this.depth < this.tiles.length){
			exit=false;
		}
		
		// check whether tile is on field
		if (!exit && this.board.getCell(pos[0], pos[1]).isUsed()) {
			// get tile and rotation
			startTile = this.board.getCell(pos[0], pos[1]).getTile().getID() - 1;
			used[startTile] = false;
			startRotation = this.board.getCell(pos[0], pos[1]).getRotation() + 1;
			if (startRotation > 3) {
				startRotation = 0;
				startTile++;
			}

			this.board.getCell(pos[0], pos[1]).resetCell();
		}

		// check through tiles to see if one fits
		for (int i = startTile; i < this.tiles.length && !exit; i++) {

			// if exists place tile
			if ((used[i] == false)
					&& (useRotation = check(pos[0], pos[1], i, startRotation)) > -1) {
				try {
					this.board.placeTile(pos[0], pos[1], useRotation,
							this.tiles[i]);
					exit = true;
					depth++;
					this.used[i] = true;
				} catch (PlacingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			startRotation = 0;
		}

		// else set position to last position
		if (!exit) {
			this.board.getCell(pos[0], pos[1]).resetCell();
			do {
				this.depth--;
				newPos = nextPos();
			} while (depth>-1 && this.board.getCell(newPos[0], newPos[1]).isKnown());
		}

		// check whether field is done
		if (this.depth >= this.tiles.length) {
			this.solved = true;
		} 
		
		if (this.depth < 0) {
			return false;
		} else {
			return true;
		}

	}

	/*
	 * returns position for actual depth
	 */
	private int[] nextPos() {
		int dimX = this.board.getDimensionX();
		
		// determine Position for actual depth
		int posY = this.depth / dimX;
		int posX = this.depth - (posY * dimX);

		return new int[] { posX, posY };
	}


	@Override
	public boolean isSolved() {
		return this.solved;
	}

	@Override
	public int getDepth() {
		return this.depth;
	}

	@Override
	public Board getBoard() {
		return this.board;
	}
}
