package algorithm;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import exceptions.PlacingException;
import basics.*;

@SuppressWarnings("serial")

/**
 * Implementation of a simple non-recursive backtracking algorithm
 */
public class SimpleBacktracker extends Algorithm {
	private static final Logger LOGGER =
		Logger.getLogger(SimpleBacktracker.class);
	
	private CheckedBoard internalBoard;
		
	private Tile tilesById[];
	
	private int stepIndex;
	
	private boolean tileUsed[];
	
	private ArrayList<TilePlacing> placings;
	
	private class TilePlacing {
		int posX;
		int posY;
		int rotation;
		Tile tile;
		
		/**
		 * 
		 * @param posX
		 * @param posY
		 * @param rotation
		 * @param tile
		 */
//		TilePlacing(int posX, int posY, int rotation, Tile tile) {
//			this.posX = posX;
//			this.posY = posY;
//			this.rotation = rotation;
//			this.tile = tile;
//		}
		
		/**
		 * 
		 * @param pos
		 * @param rotation
		 * @param tile
		 */
		TilePlacing(int pos[], int rotation, Tile tile) {
			this.posX = pos[0];
			this.posY = pos[1];
			this.rotation = rotation;
			this.tile = tile;
		}
		
		public int[] getPos() {
			return new int[]{posX, posY};
		}

		public int getRotation() {
			return rotation;
		}

		public Tile getTile() {
			return tile;
		}
		
		public int getPosX() {
			return posX;
		}
		
		public int getPosY() {
			return posY;
		}
	}
	
	public SimpleBacktracker(Board board, Tile[] tiles, Hint[] hints)
			throws PlacingException {
		stepIndex = 0;
		tileUsed = new boolean[tiles.length + 1];
		tilesById = new Tile[tiles.length + 1];
		
		// mark all tiles as unused 
		for(int i = 1; i < tileUsed.length; i++) {
			tileUsed[i] = false;
			tilesById[i] = tiles[i - 1];
		}
				
		placings = new ArrayList<TilePlacing>(tiles.length);
		
		// init board
		internalBoard = new CheckedBoard(new EternityBoard(
				board.getDimensionX(), board.getDimensionY()));
		
		ArrayList<Tile> remainingTiles = new ArrayList<Tile>();
		ArrayList<Tile> usedTiles = new ArrayList<Tile>();
				
		// check and set all hints
		for(Hint hint : hints) {
			// TODO: fix: this crashes with bad IDs
			Tile matchingTile = tilesById[hint.getID()];
			if(matchingTile == null) {
				LOGGER.warn("Bad hint (Id: " + hint.getID() + ", X:"
						+ hint.getPosX() + ", Y: " + hint.getPosY() + ", Rot: "
						+ hint.getRotation());
				continue;
			}
			internalBoard.placeTile(hint.getPosX(), hint.getPosY(),
					hint.getRotation(), matchingTile);
			usedTiles.add(matchingTile);
			
			tileUsed[matchingTile.getID()] = true;
			
			stepIndex++;
		}
		
		// add all non-hints to remainingTiles
		for(Tile tile : tiles) {
			if(!usedTiles.contains(tile)) {
				remainingTiles.add(tile);
			}
		}
		
		// determine the first unused cell
		int initialFreePosition[] = getFirstEmptyCell(internalBoard);
		
		if(initialFreePosition == null) {
			// no free cells left - this board is already solved
			return;
		}
		
		// determine the initial element on the stack
		for(int i = 1; i < tileUsed.length; i++) {
			if(!tileUsed[i]) {
				for(int rotation = 0; rotation < 4; rotation++) {
					try {
						internalBoard.placeTile(initialFreePosition[0],
								initialFreePosition[1], rotation,
								tilesById[i]);
						placings.add(new TilePlacing(initialFreePosition,
								rotation, tilesById[i]));
						tileUsed[i] = true;
						return;
					} catch(PlacingException e) {
						// could not construct a configuration - simply continue
						// with the next try
					}
				}
			}
		}
	}

	@Override
	public boolean calculateStep() {
		stepIndex++;
		
		if(stepIndex % 10000 == 0) {
			LOGGER.info("Steps: " + stepIndex);
		}
		
		if(placings.isEmpty()) {			
			// no more configs to test
			return false;
		}
		
		// determine next free position on the board
		int nextFreePos[] = getNextEmptyCell(internalBoard,
				placings.get(placings.size() - 1).getPos());
		
		// no more free positions - the board obviously must be solved
		if(nextFreePos == null) {
//			this.solution = internalBoard;
			return false;
		}
		
		// try and find a tile to put on the next free position
		for(int i = 1; i < tileUsed.length; i++) {
			if(!tileUsed[i]) {
				List<Integer> matchingRotations =
					internalBoard.getMatchingRotations(nextFreePos[0],
							nextFreePos[1], tilesById[i]);
				for(int rotation : matchingRotations) {
					try {
						internalBoard.placeTile(nextFreePos[0],
								nextFreePos[1],	rotation,
								tilesById[i]);
						// tile fits on the board
						// increment stack and return
						placings.add(new TilePlacing(nextFreePos, rotation,
								tilesById[i]));
						tileUsed[i] = true;
						return true;
					} catch (PlacingException e) {
						// tile cannot be placed with those parameters
						// simply continue
					}
				}
			}
		}

		// no tile can be put on the next free position
		
		while(placings.size() > 0) {
			// try and replace the current tile
			// starting with a greater rotation
			TilePlacing toBeDropped = placings.remove(placings.size() - 1);
			tileUsed[toBeDropped.getTile().getID()] = false;
			internalBoard.removeTile(toBeDropped.getPosX(),
					toBeDropped.getPosY());

			List<Integer> matchingRotations =
				internalBoard.getMatchingRotations(toBeDropped.getPosX(),
						toBeDropped.getPosY(), toBeDropped.getRotation() + 1,
						toBeDropped.getTile());
			for(int rotation : matchingRotations) {
				try {
					internalBoard.placeTile(toBeDropped.getPosX(),
							toBeDropped.getPosY(), rotation,
							toBeDropped.getTile());
					placings.add(new TilePlacing(toBeDropped.getPos(),
							rotation, toBeDropped.getTile()));
					tileUsed[toBeDropped.getTile().getID()] = true;
					return true;
				} catch (PlacingException e) {
					// tile cannot be placed with the selected parameters
					// simply continue
				}
			}
			
			// greater rotation didn't work out
			// try a tile with higher ID
			for(int i = toBeDropped.getTile().getID() + 1;
					i < tileUsed.length; i++) {
				if(!tileUsed[i]) {
					matchingRotations =
						internalBoard.getMatchingRotations(
								toBeDropped.getPosX(), toBeDropped.getPosY(),
								tilesById[i]);
					for(int rotation : matchingRotations) {
						try {
							internalBoard.placeTile(toBeDropped.getPosX(),
									toBeDropped.getPosY(), rotation, 
									tilesById[i]);
							placings.add(new TilePlacing(toBeDropped.getPos(),
									rotation, tilesById[i]));
							tileUsed[i] = true;
							return true;
						} catch (PlacingException e) {
							// tile cannot be placed with the selected
							// parameters
							// simply continue
						} 
					}
				}
			}

			// replacing the top-most tile on the stack also failed
			// re-iterate the replacing attempts			
		}

		return false;
	}

	@Override
	public boolean isSolved() {
		return TestSolution.isSolution(internalBoard);
	}

	@Override
	public int getDepth() {
		return internalBoard.getNumberOfPlacedTiles();
//		int tileCount = 0;
//		for(int i = 1; i < tileUsed.length; i++) {
//			if(tileUsed[i]) {
//				tileCount++;
//			}				
//		}
//		return tileCount;
	}
	
	/**
	 * Determine the coordinates of the first unused cell as int[2]
	 * @param board
	 * @return the coordinate of the first unused cell or null if there is
	 * no unused cell
	 */
	private static int[] getFirstEmptyCell(Board board) {
//		for(int j = 0; j < board.getDimensionY(); j++) {
//			for(int i = 0; i < board.getDimensionX(); i++) {
//				Cell currentCell = board.getCell(i, j); 
//				if(currentCell != null && !currentCell.isUsed()) {
//					return new int[]{i, j};
//				}
//			}
//		}
//		return null;
		return getNextEmptyCell(board, null);
	}
	
	/**
	 * Determine the coordinates of the next unused cell as int[2]; the first
	 * checked position is the one following startPosition
	 * @param board
	 * @param startPosition the position to start the search from; may be null
	 * in which case the search starts at (0, 0)
	 * @return the coordinate of the next unused cell or null if there is no
	 * unused cell after startPosition
	 */
	private static int[] getNextEmptyCell(Board board, int[] startPosition) {
//		int startX = startPosition != null ? startPosition[0] + 1 : 0;
		int startY = startPosition != null ? startPosition[1] : 0;
		for(int j = startY; j < board.getDimensionY(); j++) {
			for(int i = 0; i < board.getDimensionX(); i++) {
//			for(int i = startX; i < board.getDimensionX(); i++) {
				Cell currentCell = board.getCell(i, j); 
				if(!currentCell.isUsed()) {
					return new int[]{i, j};
				}
			}
		}
		return null;
	}

	@Override
	public Board getBoard() {
		return this.internalBoard;
	}
}
