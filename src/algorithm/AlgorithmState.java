package algorithm;

import java.io.*;
import java.util.HashMap;

import basics.Board;


public abstract class AlgorithmState implements Serializable{
	private static final long serialVersionUID = -2947456223692692679L;

	/**
	 * Checks whether recent depth is new (recent) Highscore or new lowest Backtrack
	 * accordingly set's value
	 * @param depth
	 */
	public abstract void checkDepth(int depth);
	
	/**
	 * returns most recent Highscore
	 * does not have to be actual Highscore
	 * @return
	 */
	public abstract int getRecentHighscore();
	
	/**
	 * returns actual Highscore
	 * @return
	 */
	public abstract int getHighscore();
	
	/**
	 * returns most recent Backtrackdepth
	 * @return
	 */
	public abstract int getRecentBacktrack();
	
	/**
	 * returns count of Steps taken
	 * @return
	 */
	public abstract long getStepCount();
	
	/**
	 * sets Starttime
	 */
	public abstract void startCalculation();
	
	/**
	 * returns Time the Algorithm is running in milliseconds
	 * @return
	 */
	public abstract long getCalculationTime();

	/**
	 * return the actual highscore board
	 * @return
	 */
	public abstract Board getHighscoreBoard();

	/**
	 * return map of highscores higher than border
	 * @return
	 */
	public abstract HashMap<Integer, Board> getHighscoreBoardMap();
	
	/**
	 * returns calulated value for the time needed to find the next highscore
	 * @return
	 */
	public abstract long getTimeToNextHighscore();
}
