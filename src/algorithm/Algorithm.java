package algorithm;

import basics.Board;

import java.io.*;

import exceptions.AlgorithmException;

public abstract class Algorithm implements Serializable{
	private static final long serialVersionUID = 1004655389425228281L;

	/**
	 * Implements the Algorithm to solve the Puzzle
	 * @return true if solution found
	 * @throws AlgorithmException 
	 */
	public abstract boolean calculateStep();
	
	/**
	 * returns Status of puzzle
	 * @return
	 */
	public abstract boolean isSolved();
	
	/**
	 * returns actual depth
	 * @return
	 */
	public abstract int getDepth();
	
	/**
	 * returns the current board
	 * @return
	 */
	public abstract Board getBoard();
	
}
