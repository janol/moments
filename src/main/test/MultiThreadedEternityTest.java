package main.test;

import static org.junit.Assert.*;

import java.io.File;
import main.*;

import org.junit.*;

import exceptions.AlgorithmInstantiationException;

import basics.Board;
import basics.Log4jConfigurator;
import basics.TestSolution;

public class MultiThreadedEternityTest {
	private MultiThreadedEternity mTE;
	private Board board;
	
	@Before
	public void setUp() {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
	}
	
	@Test
	public void testCalculate() throws AlgorithmInstantiationException {
		this.mTE = new MultiThreadedEternity("resources" + File.separator + "tiles4x4.txt",
				"resources" + File.separator + "hints4x4.txt", 4, 4, "algorithm.BacktrackAlgorithm");
		this.mTE.calculate();
		
		this.board = this.mTE.getSolution();
		
		assertTrue(TestSolution.isSolution(this.board));
	}
	
	//TODO simulate press return; how to test this?
	@Ignore
	@Test
	public void testCalculate10x10() throws AlgorithmInstantiationException {
		this.mTE = new MultiThreadedEternity("resources" + File.separator + "tiles10x10.txt",
				"resources" + File.separator + "hints10x10.txt", 10, 10, "BacktrackAlgorithm");
		this.mTE.calculate();
		
		this.board = this.mTE.getSolution();
		
	}
	
	@Test
	public void testResumeState() throws InterruptedException {
		this.mTE = new MultiThreadedEternity("resources" + File.separator + "state10x10.e2");
		
		this.mTE.calculate();
		
		do {
			Thread.sleep(1000);
		} while (this.mTE.getSolution() == null);
		
		this.board = this.mTE.getSolution();
		
		assertTrue(TestSolution.isSolution(this.board));
	}

	@Test
	public void testGetSolution() {
		fail("Not yet implemented");
	}

	@Test
	public void testKeyPressed() {
		fail("Not yet implemented");
	}

}
