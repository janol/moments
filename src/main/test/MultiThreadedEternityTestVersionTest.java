package main.test;

import static org.junit.Assert.*;

import java.io.*;

import main.*;

import org.junit.*;

import basics.Board;
import basics.Log4jConfigurator;
import basics.TestSolution;

import exceptions.AlgorithmInstantiationException;
import exceptions.PlacingException;

public class MultiThreadedEternityTestVersionTest {
	private MultiThreadedEternityTestVersion mTE;
	private Board board;

	@Before
	public void setUp() {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
	}

	@Ignore @Test
	public void testCalculateBacktrackCreateFile() throws PlacingException, AlgorithmInstantiationException {
		this.mTE = new MultiThreadedEternityTestVersion("resources" + File.separator + "tiles4x4.txt",
				"resources" + File.separator + "hints4x4.txt", 4, 4, "algorithm.BacktrackAlgorithm");
		this.mTE.calculate();
		
		File f = new File("solution.txt");
		assertTrue(f.exists());
		f.delete();
	}
	
	@Test
	public void testCalculateBacktrackPasses() throws PlacingException, AlgorithmInstantiationException {
		this.mTE = new MultiThreadedEternityTestVersion("resources" + File.separator + "tiles4x4.txt",
				"resources" + File.separator + "hints4x4.txt", 4, 4, "algorithm.BacktrackAlgorithm");
		this.mTE.calculate();
		
		this.board = this.mTE.getSolution();
		assertTrue(this.board!=null);
	}
	
	@Test
	public void testCalculateBacktrackFail() throws PlacingException, AlgorithmInstantiationException {
		this.mTE = new MultiThreadedEternityTestVersion("resources" + File.separator + "tilesFail4x4.txt",
				"resources" + File.separator + "hintsFail4x4.txt", 4, 4, "algorithm.BacktrackAlgorithm");
		this.mTE.calculate();
		
		this.board = this.mTE.getSolution();
		assertTrue(this.board==null);
	}
	
	@Test
	public void testSolution4x4() throws PlacingException, AlgorithmInstantiationException{
		this.mTE = new MultiThreadedEternityTestVersion("resources" + File.separator + "tiles4x4.txt",
				"resources" + File.separator + "hints4x4.txt", 4, 4, "algorithm.BacktrackAlgorithm");
		this.mTE.calculate();
		
		this.board = this.mTE.getSolution();
		
		assertTrue(TestSolution.isSolution(this.board));
	}
	
	@Test
	@Ignore
	public void testSolution10x10() throws IOException, PlacingException, AlgorithmInstantiationException{
//		File logFile = new File("C:\\temp\\log10x10.txt");
//		if(logFile.exists()) {
//			logFile.delete();
//		}
//		Logger.getRootLogger().addAppender(new FileAppender(new PatternLayout(
//				"%d{dd MMM yyy HH:mm:ss} [%t] %-5p %c %x - %m%n"), logFile.getCanonicalPath()));
		this.mTE = new MultiThreadedEternityTestVersion("resources" + File.separator + "tiles10x10.txt",
				"resources" + File.separator + "hints10x10.txt", 10, 10, "algorithm.BacktrackAlgorithm");
		this.mTE.calculate();
		
		this.board = this.mTE.getSolution();
		
		assertTrue(TestSolution.isSolution(this.board));
	}
}
