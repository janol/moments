package main.test;

import static org.junit.Assert.*;

import java.io.*;

import org.junit.*;

import basics.Board;
import basics.Log4jConfigurator;
import basics.TestSolution;

import exceptions.AlgorithmInstantiationException;
import exceptions.PlacingException;

import main.*;

public class SingleThreadedEternityTest {
	private SingleThreadedEternity sTE;
	private Board board;

	@Before
	public void setUp() {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
	}

	@Test
	public void testCalculateBacktrackCreateFile() throws PlacingException, AlgorithmInstantiationException {
		this.sTE = new SingleThreadedEternity("resources" + File.separator
				+ "tiles4x4.txt", "resources" + File.separator + "hints4x4.txt", 4,
				4, "algorithm.BacktrackAlgorithm");
		this.sTE.calculate();
		
		File f = new File("solution.txt");
		assertTrue(f.exists());
		f.delete();
	}
		
	@Test
	public void testCalculateSimpleAlgoTest() throws PlacingException, AlgorithmInstantiationException {
		this.sTE = new SingleThreadedEternity("resources" + File.separator
				+ "tiles2x2.txt", "resources" + File.separator + "hints2x2.txt", 2,
				2, "algorithm.BacktrackAlgorithm");
	}
	
	@Test
	public void testCalculateBacktrackPasses() throws PlacingException, AlgorithmInstantiationException {
		this.sTE = new SingleThreadedEternity("resources" + File.separator
				+ "tiles4x4.txt", "resources" + File.separator + "hints4x4.txt", 4,
				4, "algorithm.BacktrackAlgorithm");
		this.sTE.calculate();
		
		this.board = this.sTE.getSolution();
		assertTrue(this.board!=null);
	}
	
	@Test
	public void testCalculateBacktrackFail() throws PlacingException, AlgorithmInstantiationException {
		this.sTE = new SingleThreadedEternity("resources" + File.separator
				+ "tilesFail4x4.txt", "resources" + File.separator
				+ "hintsFail4x4.txt", 4, 4, "algorithm.BacktrackAlgorithm");
		this.sTE.calculate();
		
		this.board = this.sTE.getSolution();
		assertFalse(TestSolution.isSolution(this.board));
	}
	
	@Test
	public void testCalculateBacktrackPassesCorners() throws PlacingException, AlgorithmInstantiationException {
		this.sTE = new SingleThreadedEternity("resources" + File.separator
				+ "tiles4x4.txt", "resources" + File.separator
				+ "hintsCorners4x4.txt", 4, 4, "algorithm.BacktrackAlgorithm");
		this.sTE.calculate();
		
		this.board = this.sTE.getSolution();
		assertTrue(this.board!=null);
	}
	
	@Test
	public void testSolution() throws PlacingException, AlgorithmInstantiationException{
		this.sTE = new SingleThreadedEternity("resources/tiles4x4.txt",
				"resources/hintsCorners4x4.txt", 4, 4, "algorithm.BacktrackAlgorithm");
		this.sTE.calculate();
		
		this.board = this.sTE.getSolution();
		
		assertTrue(TestSolution.isSolution(this.board));
	}
	
	//can't push file due to copyright reasons
	@Ignore
	@Test
	public void testCluePuzzle6x12() throws PlacingException, AlgorithmInstantiationException {
		this.sTE = new SingleThreadedEternity("resources" + File.separator
				+ "X72-Teile.txt", "resources" + File.separator + "hints4x4.txt", 12,
				6, "algorithm.BacktrackAlgorithm");
		this.sTE.calculate();
		
		this.board = this.sTE.getSolution();
		assertTrue(this.board!=null);
	}
}
