package main;

import io.EternityBoardSaver;
import io.EternityTileReader;

import java.io.IOException;

import org.apache.log4j.Logger;

import exceptions.AlgorithmInstantiationException;
import exceptions.EmptyFileException;
import exceptions.HintException;
import exceptions.NoFileException;
import exceptions.PlacingException;
import basics.Log4jConfigurator;
import solver.EternityThreadBasedSolver;
import basics.*;

public class MultiThreadedEternityTestVersion {
	private static final Logger LOGGER =
		Logger.getLogger(MultiThreadedEternityTestVersion.class);
		
	private EternityThreadBasedSolver eternitySolver;
	private Tile[] eternityTiles = null;
	private Hint[] eternityHints = null;
	private EternityTileReader eternityReader;
	private Board eternityBoard;
	private Board solution = null;
	
	public MultiThreadedEternityTestVersion(String tileFile, String hintFile,
			int dimX, int dimY, String algorithmType) throws PlacingException,
			AlgorithmInstantiationException {
		this.eternityReader = new EternityTileReader();
		
		try{
			try {
				this.eternityTiles = this.eternityReader.getTiles(tileFile);
			} catch (EmptyFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.eternityHints = this.eternityReader.getHints(hintFile);
		} catch (IOException e){
			LOGGER.error("File not found: "+e.getMessage());
		} catch (HintException e){
			LOGGER.error(e.getMessage());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.eternityBoard = new EternityBoard(dimX, dimY);
		
		this.eternitySolver = new EternityThreadBasedSolver(algorithmType,
				this.eternityBoard, this.eternityTiles, this.eternityHints);
	}
	
	public void calculate() {
		this.solution = this.eternitySolver.calculateSolution();
		if (this.solution != null){
			EternityBoardSaver.saveBoard(this.solution, "Solution.txt");
			LOGGER.info("Solution found and saved to Solution.txt");
		} else{
			LOGGER.info("no Solution found");
		}
	}
	
	public Board getSolution(){
		return this.solution;
	}
	
	public static void main(String[] args) throws PlacingException,
			NumberFormatException, AlgorithmInstantiationException {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
		
		if(args.length < 4) {
			System.err.println("Usage: <tileFile> <hintFile> <dimX> <dimY>");
			return;
		}
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
		
		MultiThreadedEternityTestVersion mTE = new MultiThreadedEternityTestVersion(args[0],
				args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]),
				"BacktrackAlgorithm");
		mTE.calculate();
	}
}
