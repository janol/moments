package main;

import io.EternityBoardSaver;
import io.EternityTileReader;
import io.StateSaver;

import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import exceptions.AlgorithmInstantiationException;
import exceptions.EmptyFileException;
import exceptions.HintException;
import exceptions.NoFileException;
import exceptions.PlacingException;

import basics.Log4jConfigurator;
import solver.EternityThreadBasedSolver;
import threading.AlgorithmThread;

import basics.*;

//later the main
public class MultiThreadedEternity implements KeyListener {
	/*
	 * Time waiting till safe state
	 */
	public final int SAFE_TIME = 100*60*10;
	public final int SLICE_TIME = 100;
	
	private static final Logger LOGGER =
		Logger.getLogger(MultiThreadedEternityTestVersion.class);
	
	// used to stop calculation manually
	private boolean userAbort = false;
	
	private EternityThreadBasedSolver eternitySolver = null;
	private EternityTileReader eternityReader = null;
	private Tile[] eternityTiles;
	private Hint[] eternityHints;
	private EternityBoard eternityBoard;
	private Board solution = null;
	
	public MultiThreadedEternity(String tileFile, String hintFile, int dimX, int dimY
			, String algorithmType)
			throws AlgorithmInstantiationException {

		//load tiles/boards
		this.eternityReader = new EternityTileReader();
		
		try{
			try {
				this.eternityTiles = this.eternityReader.getTiles(tileFile);
			} catch (EmptyFileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.eternityHints = this.eternityReader.getHints(hintFile);
		} catch (IOException e){
			LOGGER.error("File not found: "+e.getMessage());
		} catch (HintException e){
			LOGGER.error(e.getMessage());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.eternityBoard = new EternityBoard(dimX, dimY);
		
		try {
			this.eternitySolver = new EternityThreadBasedSolver(algorithmType,
					this.eternityBoard, this.eternityTiles, this.eternityHints);
		} catch (PlacingException e) {
			LOGGER.info("Please enter correct hints");
			e.printStackTrace();
		}

	}
	
	@SuppressWarnings("unchecked")
	public MultiThreadedEternity(String state) {
			Object tmpAlgo = StateSaver.getObject(state);
			eternitySolver = new EternityThreadBasedSolver((ArrayList<AlgorithmThread>) tmpAlgo);
	}
	
	/**
	 * calculates the field
	 * @return if a solution is found
	 * @throws IOException 
	 */
	public boolean calculate() {
		long startTime = System.currentTimeMillis();
		
		//starts calculation
		Thread thread = new Thread(eternitySolver);
		thread.start();
		
		LOGGER.info("To stop please press ENTER");
		//poll till user aborts or solution found
		do {
			// wait slicetime 
			try {
				Thread.sleep(SLICE_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// save clone
			if (System.currentTimeMillis() > (startTime + SAFE_TIME)){
				eternitySolver.pauseCalculation();
				Object state = eternitySolver.getAlgorithmStates();
				eternitySolver.resumeCalculation();
				StateSaver.saveObject(state, "tmp" + File.separator + "state.e2");
				startTime = System.currentTimeMillis();
			}
			
			//TODO workaround; try changing to events
			try {
				if (System.in.available() > 0) {
					userAbort = true;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} while ((!userAbort) && (thread.getState() != Thread.State.TERMINATED));
		
		if (userAbort) {
			eternitySolver.stopThreads();
			
			LOGGER.info("Calculation stoped by user");
			return false;
		}
		
		this.solution = eternitySolver.getSolution();
		return true;
	}
	
	/**
	 * returns the solution found
	 * @return
	 */
	public Board getSolution() {
		return eternitySolver.getSolution();
	}
	
	public static void main(String[] args) throws NumberFormatException,
			AlgorithmInstantiationException {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
		
		if(args.length < 4) {
			System.err.println("Usage: <tileFile> <hintFile> <dimX> <dimY> <status>");
			return;
		}
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
		
		MultiThreadedEternity mTE = new MultiThreadedEternity(args[0],
				args[1], Integer.parseInt(args[2]), Integer.parseInt(args[3]),
				"algorithm.BacktrackAlgorithm");
		mTE.calculate();
		
		
		//TODO if has to be rewritten since change to getBoard
		if (mTE.solution != null){
			EternityBoardSaver.saveBoard(mTE.solution, "Solution.txt");
			LOGGER.info("Solution found and saved to Solution.txt");
		} else{
			LOGGER.info("no Solution found");
		}
	}

	public void keyPressed(KeyEvent e) {
		userAbort = true;
	}

	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}
