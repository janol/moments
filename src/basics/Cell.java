package basics;

import java.io.*;

import exceptions.PlacingException;

public abstract class Cell implements Serializable{
	private static final long serialVersionUID = -6528665022351955068L;

	/**
	 * Returns Tile in Cell
	 * @return
	 */
	public abstract Tile getTile();
	
	/**
	 * returns rotation of tile stored in cell
	 * @return
	 */
	public abstract int getRotation();
	
	/**
	 * Places Tile to store in Cell
	 * @param t
	 */
	public abstract void setTile(Tile t);
	
	/**
	 * Set Rotation for Tile stored in Cell
	 * @param Rotation
	 */
	public abstract void setRotation(int Rotation) throws PlacingException;
	
	/**
	 * resets Cell
	 * Tile=null
	 * Rotation=0;
	 */
	public abstract void resetCell();
	
	/**
	 * returns if there is already a tile stored inside cell
	 * @return
	 */
	public abstract boolean isUsed();
	
	/**
	 * is this Cell known so don't change it
	 * @return
	 */
	public abstract boolean isKnown();
	
	/**
	 * is the Knownstatus of thsi cell
	 * @param known
	 */
	public abstract void setKnown(boolean known);
}
