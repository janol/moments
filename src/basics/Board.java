package basics;

import java.io.*;

import basics.EternityCell;
import basics.EternityTile;
import exceptions.PlacingException;

public abstract class Board implements Serializable{
	private static final long serialVersionUID = -6589132145960371459L;

	/**
	 * Places piece on Board
	 * @param PosX
	 * @param PosY
	 * @param Rotation count of 90� clockwise rotation of tile
	 * @param Piece
	 * @throws PlacingException
	 */
	public abstract void placeTile(int PosX, int PosY, int Rotation, Tile Piece) 
		throws PlacingException;
	
	/**
	 * Returns tile at position(PosX, PosY)
	 * @param PosX
	 * @param PosY
	 * @return
	 */
	public abstract Tile getTile(int PosX, int PosY);
	
	/**
	 * Returns rotation of Tile at position(PosX, PosY)
	 * @param PosX
	 * @param PosY
	 * @return
	 */
	public abstract int getRotation(int PosX, int PosY);
	
	/**
	 * returns Cell at position(posX, posY)
	 * Cells include Tile and Rotation
	 * @param PosX
	 * @param PosY
	 * @return
	 */
	public abstract EternityCell getCell(int PosX, int PosY);
	
	/**
	 * returns if this Position is a CluePiece
	 * @param PosX
	 * @param PosY
	 * @return
	 */
	public abstract boolean isKnown(int PosX, int PosY);
	
	/**
	 * Sets Cluestatus of this Cell
	 * @param PosX
	 * @param PosY
	 * @param known
	 * @see Cell#isKnown()
	 * @see Cell#setKnown(boolean)
	 */
	public abstract void setKnown(int PosX, int PosY, boolean known);
	
	/**
	 * Get the number of columns of this board
	 * @return
	 */
	public abstract int getDimensionX();
	
	/**
	 * Get the number of rows of this board
	 * @return
	 */
	public abstract int getDimensionY();
	
	/**
	 * Get the colors required of the supplied position
	 * @param posX
	 * @param posY
	 * @return
	 * @deprecated
	 * @see Board#getColorConstraintsAsDummyTile(int, int)
	 */
	public abstract int[] getSurroundingColors(int posX, int posY);
	
	/**
	 * Get the colors required of the supplied position as dummy tile
	 * @param posX
	 * @param posY
	 * @return
	 */
	public abstract EternityTile getColorConstraintsAsDummyTile(int posX,
			int posY);
	
	/**
	 * Check whether placing the supplied tile at the defined position would
	 * violate the color constraints
	 * @param posX
	 * @param posY
	 * @param rotation
	 * @param tile
	 * @return true iff if the tile satisfies the color constraints
	 */
	public abstract boolean tileMatchesColors(int posX, int posY, int rotation,
			Tile tile);
	
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Board)) {
			return false;
		}
		
		Board b = (Board) o;
		if ((b.getDimensionX() != this.getDimensionX()) || (b.getDimensionY() != this.getDimensionY())) {
			return false;
		}
		
		for (int i = 0; i < b.getDimensionX(); i++) {
			for (int j = 0; j < b.getDimensionY(); j++) {
				Cell boardCell = b.getCell(i, j);
				Cell thisCell = this.getCell(i, j);
				if ((boardCell.getTile() == null) && (thisCell.getTile() == null)) {
					continue;
				}
				else if ((boardCell.getTile().getID() != thisCell.getTile().getID()) ||
						(boardCell.getRotation() != thisCell.getRotation()) ) {
					return false;
				}
			}
		}
		return true;
	}
}
