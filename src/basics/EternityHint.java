package basics;

import exceptions.*;

/**
 * Hints are used for known Tiles
 * Include Position, Rotation and Tile
 * @author OLJA100
 *
 */
public class EternityHint extends Hint {
	private static final long serialVersionUID = 1845068496753164438L;
	
	private int Id;
	private int PosX;
	private int PosY;
	private int Rotation;
	
	public EternityHint(int PosX, int PosY, int Id, int Rotation) throws HintException{
		if (PosX<0 || PosX>15 || PosY<0 || PosY>15)
			throw new HintException("Wrong Dimensions");
		else if (Rotation<0 || Rotation>3)
			throw new HintException("Wrong Rotation");
		else if (Id<1 || Id>256)
			throw new HintException("No Eternity ID");
		this.Id = Id;
		this.PosX = PosX;
		this.PosY = PosY;
		this.Rotation = Rotation;
	}

	@Override
	public int getID() {
		return this.Id;
	}

	@Override
	public int getPosX() {
		return this.PosX;
	}

	@Override
	public int getPosY() {
		return this.PosY;
	}

	@Override
	public int getRotation() {
		return this.Rotation;
	}

	public boolean equals(Hint h){
		boolean equal=false;
		if (h.getID() == this.Id){
			if ((h.getPosX() == this.PosX) && (h.getPosY() == this.PosY) && (h.getRotation() == this.Rotation)) {
				equal=true;
			}
		}
		return equal;
	}
}
