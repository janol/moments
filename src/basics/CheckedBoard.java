package basics;

import java.util.ArrayList;
import java.util.List;

import exceptions.PlacingException;

public class CheckedBoard extends EternityBoard {
	private static final long serialVersionUID = 6971316455905142245L;
	private int numberOfTiles;

	public CheckedBoard(Board board) throws PlacingException {
		super(board.getDimensionX(), board.getDimensionY());
		
		numberOfTiles = 0;
		
		for(int i = 0; i < board.getDimensionX(); i++) {
			for(int j = 0; j < board.getDimensionY(); j++) {
				Cell currentCell = board.getCell(i, j);
				if(currentCell.isUsed()) {
					this.placeTile(i, j, currentCell.getRotation(),
							currentCell.getTile());
				}
			}
		}
	}
	
	/**
	 * Additionally checks whether the tile preserves the color constraints and
	 * throws a PlacingException if not
	 */
	public void placeTile(int PosX, int PosY, int Rotation, Tile Piece)
			throws PlacingException {
		if(!tileMatchesColors(PosX, PosY, Rotation, Piece)) {
			throw new PlacingException("Tile violates color constraints");
		}
		
		super.placeTile(PosX, PosY, Rotation, Piece);
		
		numberOfTiles++;
	}
	
	/**
	 * Try and remove the tile from the supplied position
	 * @param posX
	 * @param posY
	 * @return the removed tile or <code>null</code> if the cell was empty
	 */
	public Tile removeTile(int posX, int posY) {
		Cell cell = getCell(posX, posY);
		
		if(!cell.isUsed()) {
			return null;
		}
		
		Tile tile = cell.getTile();
		
		cell.resetCell();
		
		numberOfTiles--;
		
		return tile;
	}
	
	public int getNumberOfPlacedTiles() {
		return numberOfTiles;
	}

	public List<Integer> getMatchingRotations(int posX, int posY, Tile tile) {
		return getMatchingRotations(posX, posY, 0, tile);
	}

	public List<Integer> getMatchingRotations(int posX, int posY,
			int startingRotation, Tile tile) {
		EternityTile surroundingColors = 
			getColorConstraintsAsDummyTile(posX, posY);
		ArrayList<Integer> rotations = new ArrayList<Integer>(4);

		for(int rot = startingRotation; rot < 4; rot++) {
			if(tile.colorsMatch(surroundingColors, rot)) {
				rotations.add(rot);
			}
		}
		return rotations;
	}
}
