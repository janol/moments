package basics;

public abstract class Choice {
	
	/**
	 * returns the Id of tile included in choice
	 * @return
	 */
	public abstract int getID();
	
	/**
	 * returns the rotation of this choice
	 * @return
	 */
	public abstract int getRotation();
}
