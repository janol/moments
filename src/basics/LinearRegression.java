package basics;

import java.util.ArrayList;

public class LinearRegression {
	public static synchronized long getNextTime(ArrayList<Long> values) {
		//values for y=ax+b
		double a = 0;
		double b = 0;
		
		//formula check gauss quadratic regression
		double[] tmpValues = new double[4];
		
		//calculate each sum itself
		for (int i = 0; i < values.size(); i++) {
			tmpValues[0] += i * Math.log(values.get(i));
			tmpValues[1] += i;
			tmpValues[2] += Math.log(values.get(i));
			tmpValues[3] += Math.pow(i, 2);
		}
		
		a = (values.size() * tmpValues[0] - tmpValues[1] * tmpValues[2]) 
				/ (values.size() * tmpValues[3] - Math.pow(tmpValues[1], 2));
		b = (tmpValues[3] * tmpValues[2] - tmpValues[1] * tmpValues[0]) 
				/ (values.size() * tmpValues[3] - Math.pow(tmpValues[1], 2));
		
		double nextValue = a * (values.size() + b);
		
		return (long) Math.exp(nextValue);
	}
}
