package basics;

import java.io.*;

/**
 * Clones the Object given
 * Has to be serializable
 * @author OLJA100
 *
 */
public class CloneObject {
	/**
	 * returns DeepCopy ob object o
	 * @param o
	 * @return
	 * @throws Exception
	 */
	public static Object deepCopy( Object o ) throws Exception 
	{ 
	  ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
	  new ObjectOutputStream( baos ).writeObject( o ); 
	  ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray() ); 
	  return  new ObjectInputStream( bais ).readObject(); 
	}
}
