package basics;

public class EternityTile extends Tile {
	private static final long serialVersionUID = -1437244135161329906L;
	
	private int Id;
	private int[] Colors;
	
	/**
	 * An ID not valid for normal tiles and used only for dummy-tiles
	 */
	private static final int DUMMY_ID = -2;
	
	/**
	 * Create a new tile
	 * TODO: implement checking of Id and colors
	 * @param Id
	 * @param topColor
	 * @param rightColor
	 * @param bottomColor
	 * @param leftColor
	 */
	public EternityTile(int Id, int topColor, int rightColor,
			int bottomColor, int leftColor) {
		this.Id = Id;
		
		this.Colors = new int[Side.values().length];
		this.Colors[Side.TOP.get()] = topColor;
		this.Colors[Side.RIGHT.get()] = rightColor;
		this.Colors[Side.BOTTOM.get()] = bottomColor;
		this.Colors[Side.LEFT.get()] = leftColor;
	}
	
	/**
	 * Create a new dummy tile by supplied colors; dummy-tiles don't allow
	 * for specifying an ID
	 * @param topColor a positive int or ANY_COLOR
	 * @param rightColor a positive int or ANY_COLOR
	 * @param bottomColor a positive int or ANY_COLOR
	 * @param leftColor a positive int or ANY_COLOR
	 * @return
	 */
	public static EternityTile createDummyTile(int topColor, int rightColor,
			int bottomColor, int leftColor) {
		return new EternityTile(DUMMY_ID, topColor, rightColor, bottomColor,
				leftColor);
	}
	
	@SuppressWarnings("unused")
	private EternityTile() {
	}

	@Override
	public int getColor(Side side) {
		return this.Colors[side.get()];
	}

	@Override
	public int getID() {
		return this.Id;
	}

	@Override
	public int getRotatedColor(Side side, int Rotation) {
		int rot=side.get()-Rotation;
		if (rot<0) rot+=4;
		
		return this.Colors[rot];
	}

	@Override
	public boolean colorsMatch(Tile otherTile, int rotation) {
		for(Side side : Side.values()) {
			int colorA = getRotatedColor(side, rotation);
			int colorB = otherTile.getColor(side); 
			if(colorA != Tile.ANY_COLOR && colorB != Tile.ANY_COLOR 
					&& colorA != colorB) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean isDummyTile() {
		return Id == DUMMY_ID;
	}
}
