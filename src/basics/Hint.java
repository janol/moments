package basics;

import java.io.Serializable;

public abstract class Hint implements Serializable {
	private static final long serialVersionUID = 3646819870837951038L;

	/**
	 * returns Tile ID
	 * @return
	 */
	public abstract int getID();
	
	/**
	 * returns Rotation of Tile at CluePosition
	 * @return
	 */
	public abstract int getRotation();
	
	/**
	 * returns PosX of clue
	 * @return
	 */
	public abstract int getPosX();
	
	/**
	 * returns PosY of clue
	 * @return
	 */
	public abstract int getPosY();
}
