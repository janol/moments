package basics;

import org.apache.log4j.PropertyConfigurator;

public class Log4jConfigurator {
	private static boolean configured = false;
	public static boolean isConfigured() {
		return configured;
	}
	public static void configure() {
		PropertyConfigurator.configure("conf/log4j.properties");
		configured = true;
	}
}
