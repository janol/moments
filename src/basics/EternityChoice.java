package basics;

/**
 * Includes Choices. Choices Are meant to keep track of placed Tiles
 * Rotation and Tile
 * @author OLJA100
 *
 */
public class EternityChoice extends Choice {
	private int tileId;
	private int tileRotation;
	
	public EternityChoice(Tile tile, int rotation){
		this.tileId = tile.getID();
		this.tileRotation = rotation;
	}
	@Override
	public int getID() {
		return this.tileId;
	}

	@Override
	public int getRotation() {
		return this.tileRotation;
	}

}
