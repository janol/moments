package basics;

import java.util.*;

import exceptions.HintException;
import basics.Tile.Side;

public class HintGenerator {
	public static final int BORDER_COLOR = 0;

	/**
	 * returns ProblemSets
	 * 
	 * @param tiles
	 * @param hints
	 * @param dimX
	 * @param dimY
	 * @return
	 */
	public static ArrayList<Hint[]> generateProblemSets(Tile[] tiles,
			Hint[] hints, int dimX, int dimY) {
		ArrayList<Hint[]> problemSets = new ArrayList<Hint[]>();
		
		ArrayList<Integer[]> cornerPositions = defineCorners(dimX, dimY);
		ArrayList<Tile> cornerTiles = getCorners(tiles);
		checkHintCorners(cornerPositions, cornerTiles, hints);
		ArrayList<Choice> rotatedCornerTiles = getRotatedCornerTiles(cornerTiles);
		HintGenerator.getCornerProblemSets(rotatedCornerTiles,
				cornerPositions, new ArrayList<Hint>(), problemSets);
		return problemSets;
	}

	/**
	 * returns CornerPositions
	 * 
	 * @param dimX
	 * @param dimY
	 * @return
	 */
	public static ArrayList<Integer[]> defineCorners(int dimX, int dimY) {
		ArrayList<Integer[]> cornerPositions = new ArrayList<Integer[]>();
		cornerPositions.add(new Integer[] { 0, 0 });
		cornerPositions.add(new Integer[] { 0, dimY - 1 });
		cornerPositions.add(new Integer[] { dimX - 1, 0 });
		cornerPositions.add(new Integer[] { dimX - 1, dimY - 1 });
		return cornerPositions;
	}

	/**
	 * returns CornerTiles out of set tiles max 4 CornerTiles
	 * 
	 * @param tiles
	 * @return
	 */
	public static ArrayList<Tile> getCorners(Tile[] tiles) {
		ArrayList<Tile> corners = new ArrayList<Tile>();
		int borderCount = 0;

		// check each tile for 2 Border_Colors
		for (int i = 0; (i < tiles.length) && (corners.size() < 4); i++) {
			borderCount = 0;
			for (Side s : Side.values()) {
				if (tiles[i].getColor(s) == BORDER_COLOR) {
					borderCount++;
				}
			}
			if (borderCount > 1) {
				corners.add(tiles[i]);
			}
		}
		return corners;
	}

	/**
	 * checks whether a hint is in a cornerPosition
	 * 
	 * @param cornerPosition
	 * @param corners
	 * @param hints
	 * @return
	 */
	public static void checkHintCorners(
			ArrayList<Integer[]> cornerPosition, ArrayList<Tile> corners,
			Hint[] hints) {
		ArrayList<Tile> removableTiles = new ArrayList<Tile>();
		ArrayList<Integer[]> removablePositions = new ArrayList<Integer[]>();
		for (int i = 0; i < corners.size(); i++){
			Tile t = corners.get(i);
			for (int j = 0; j < hints.length; j++){
				if (t.getID() == hints[j].getID()){
					removableTiles.add(t);
					
					for (int k = 0; k < cornerPosition.size(); k++){
						Integer[] pos = cornerPosition.get(k);
						if ((hints[j].getPosX() == pos[0]) && (hints[j].getPosY() == pos[1])){
							removablePositions.add(pos);
						}
					}
				}
			}
		}
		
		//remove Tiles
		for (int i = 0; i < removableTiles.size(); i++){
			corners.remove(removableTiles.get(i));
		}
		
		//remove Positions
		for (int i = 0; i < removablePositions.size(); i++){
			cornerPosition.remove(removablePositions.get(i));
		}
	}

	/**
	 * returns sets of hints with all corners placed
	 * 
	 * @param basicCorners
	 * @param emptyCorners
	 * @param placedHints
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static void getCornerProblemSets(
			ArrayList<Choice> basicCorners, ArrayList<Integer[]> emptyCorners,
			ArrayList<Hint> placedHints, ArrayList<Hint[]> starterHints) {
		if (basicCorners.size() == 0) {
			Hint[] tmpHint = new Hint[placedHints.size()];
			starterHints.add(placedHints.toArray(tmpHint));
			return;
		}

		for (Integer[] cell : emptyCorners) {
			Choice cornerChoice = basicCorners.get(0);
			ArrayList<Choice> modifiedBasicCorners = 
				(ArrayList<Choice>) basicCorners.clone();
			modifiedBasicCorners.remove(cornerChoice);

			ArrayList<Integer[]> modifiedEmptyCorners = 
				(ArrayList<Integer[]>) emptyCorners.clone();
			modifiedEmptyCorners.remove(cell);

			ArrayList<Hint> modifiedPlacedHints = 
				(ArrayList<Hint>) placedHints.clone();
			
			//rotation depends on position
			int rotation = cornerChoice.getRotation();
			if (cell[0] == 0){
				rotation += (cell[1] == 0) ? -1 : -2;
			}else{
				rotation += (cell[1] == 0) ? 0 : 1;
			}
			
			//make sure rotation is correct
			if (rotation > 3){
				rotation -= 4;
			}else if (rotation < 0){
				rotation += 4;
			}
			
			//add hint
			try {
				modifiedPlacedHints.add(new EternityHint(cell[0], cell[1],
						cornerChoice.getID(), rotation));
			} catch (HintException e) {
				e.printStackTrace();
			}

			getCornerProblemSets(modifiedBasicCorners,
					modifiedEmptyCorners, modifiedPlacedHints, starterHints);
		}
	}
	
	public static ArrayList<Choice> getRotatedCornerTiles(ArrayList<Tile> corners){
		ArrayList<Choice> cornerBasicRotation = new ArrayList<Choice>();
		boolean done = false;
		for (Tile t : corners) {
			done = false;
			
			//check all rotations
			for (int i = 0; i < 4 && !done; i++) {
				if (t.getRotatedColor(Side.TOP, i) == BORDER_COLOR) {
					if (t.getRotatedColor(Side.RIGHT, i) == BORDER_COLOR) {
						cornerBasicRotation.add(new EternityChoice(t, i));
					} else {
						// add one since Top is BorderColor and Left is
						// BorderColor
						cornerBasicRotation.add(new EternityChoice(t, i + 1));
					}
					done = true;
				}
			}
		}

		return cornerBasicRotation;
	}
}
