package basics;

import exceptions.*;

/**
 * Represents a Cell of The Board
 * Implements Tile and Rotation
 * @author OLJA100
 *
 */
public class EternityCell extends Cell {
	private static final long serialVersionUID = -7612908146980197655L;
	private Tile eTile = null;
	private int rotation = 0;
	private boolean known = false;
	
	@Override
	public int getRotation() {
		return this.rotation;
	}

	@Override
	public Tile getTile() {
		return this.eTile;
	}

	@Override
	public void setRotation(int rotation) throws PlacingException{
		if (rotation < 0 || rotation > 3)
			throw new PlacingException("InvalidRotation");
		this.rotation=rotation;
	}

	@Override
	public void setTile(Tile t) {
		if (t == null)
			throw new NullPointerException("No Tile");
		this.eTile = t;
	}

	@Override
	public void resetCell() {
		this.eTile = null;
		this.rotation = 0;
	}

	@Override
	public boolean isUsed() {
		return this.eTile != null;
	}

	@Override
	public boolean isKnown() {
		return this.known;
	}

	@Override
	public void setKnown(boolean known) {
		this.known = known;
	}

	

}
