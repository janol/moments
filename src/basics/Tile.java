package basics;

import java.io.*;

public abstract class Tile implements Serializable{
	private static final long serialVersionUID = 1198196321986839466L;
	
	/**
	 * the color of the border
	 */
	public static final int BORDER_COLOR = 0;
	
	/** 
	 * dummy-color where the color is irrelevant, i.e. where no tile is
	 * adjacent
	 */
	public static final int ANY_COLOR = -1;
	
	public enum Side{
		TOP(0),
		RIGHT(1),
		BOTTOM(2),
		LEFT(3);
		
		private int side;
		Side(int i){
			side=i;
		}
		
		public int get(){
			return this.side;
		}
	}
	
	/**
	 * Get the ID of this tile
	 * ID's are positive integers
	 * @return
	 */
	public abstract int getID();
	
	/**
	 * Get the color at the supplied side
	 * @param Side
	 * @return
	 */
	public abstract int getColor(Side side);
	
	/**
	 * Get the color on the supplied side when rotating the tile by 
	 * Rotation-times
	 * @param Side
	 * @param Rotation
	 * @return
	 */
	public abstract int getRotatedColor(Side side, int Rotation);
	
	/**
	 * Determine whether this tile is a dummy, i.e. has no valid ID and may
	 * have ANY_COLOR as color and therefore cannot be placed on a board
	 * @return true iff this tile is a dummy
	 */
	public abstract boolean isDummyTile();
	
	/**
	 * Determine whether the colors on this tile and <code>otherTile</code>
	 * match when applying <code>rotation</code> rotations to this tile; this
	 * as well as <code>otherTile</code> may contain ANY_COLOR if they are
	 * dummies - and ANY_COLOR of course matches any color :o)
	 * @param otherTile
	 * @param rotation
	 * @return
	 */
	public abstract boolean colorsMatch(Tile otherTile, int rotation);
}
