package basics;


import exceptions.PlacingException;

/**
 * represents the Board used later on
 * @author OLJA100
 *
 */
public class EternityBoard extends Board {
	private static final long serialVersionUID = 1945053406225667192L;
	
	//the gameboard
	protected EternityCell[][] Field;
	
	public EternityBoard(int DimX, int DimY)
	{
		this.Field = new EternityCell[DimX][DimY];
		
		//Initialize board with cells
		for (int i=0; i<DimX; i++){
			for (int j=0; j<DimY; j++)
				this.Field[i][j] = new EternityCell();
		}
	}
	
	@Override
	public void placeTile(int PosX, int PosY, int Rotation, Tile Piece)
			throws PlacingException {
		//in case of a wrong value throw an exception
		if (PosX < 0 || PosX > this.Field.length-1 || PosY < 0 || PosY > this.Field[0].length-1)
			throw new PlacingException("Wrong Dimensions");
		else if (Rotation < 0 || Rotation > 4)
			throw new PlacingException("Wrong Rotation");
		else if (Piece == null)
			throw new NullPointerException("No Tile");
		else if (Field[PosX][PosY].isUsed())
			throw new PlacingException("Already Used");
		
		Field[PosX][PosY].setTile(Piece);
		Field[PosX][PosY].setRotation(Rotation);
	}

	@Override
	public Tile getTile(int PosX, int PosY) {
		return this.Field[PosX][PosY].getTile();
	}

	public EternityCell getCell(int PosX, int PosY){
		return this.Field[PosX][PosY];
	}

	@Override
	public int getRotation(int PosX, int PosY) {
		return this.Field[PosX][PosY].getRotation();
	}

	@Override
	public boolean isKnown(int PosX, int PosY) {
		return this.Field[PosX][PosY].isKnown();
	}

	@Override
	public void setKnown(int PosX, int PosY, boolean known) {
		this.Field[PosX][PosY].setKnown(known);
	}

	@Override
	public int getDimensionX() {
		return this.Field.length;
	}

	@Override
	public int getDimensionY() {
		return this.Field[0].length;
	}

	public int[] getSurroundingColors(int posX, int posY) {
		int[] surroundColors = new int[4];
		int dimX = this.getDimensionX();
		int dimY = this.getDimensionY();

		// check top
		if (posY == 0) {
			surroundColors[Tile.Side.TOP.get()] = Tile.BORDER_COLOR;
		} else if (!getCell(posX, posY - 1).isUsed()) {
			surroundColors[Tile.Side.TOP.get()] = Tile.ANY_COLOR;
		} else {
			int rot = getCell(posX, posY - 1).getRotation();
			surroundColors[Tile.Side.TOP.get()] = getCell(posX,
					posY - 1).getTile().getRotatedColor(Tile.Side.BOTTOM, rot);
		}

		// check left
		if (posX == 0) {
			surroundColors[Tile.Side.LEFT.get()] = Tile.BORDER_COLOR;
		} else if (!getCell(posX - 1, posY).isUsed()) {
			surroundColors[Tile.Side.LEFT.get()] = Tile.ANY_COLOR;
		} else {
			int rot = getCell(posX - 1, posY).getRotation();
			surroundColors[Tile.Side.LEFT.get()] = getCell(posX - 1,
					posY).getTile().getRotatedColor(Tile.Side.RIGHT, rot);
		}

		// check bottom
		if (posY >= dimY - 1) {
			surroundColors[Tile.Side.BOTTOM.get()] = Tile.BORDER_COLOR;
		} else if (!getCell(posX, posY + 1).isUsed()) {
			surroundColors[Tile.Side.BOTTOM.get()] = Tile.ANY_COLOR;
		} else {
			int rot = getCell(posX, posY + 1).getRotation();
			surroundColors[Tile.Side.BOTTOM.get()] = getCell(posX,
					posY + 1).getTile().getRotatedColor(Tile.Side.TOP, rot);
		}

		// check right
		if (posX >= dimX - 1) {
			surroundColors[Tile.Side.RIGHT.get()] = Tile.BORDER_COLOR;
		} else if (!getCell(posX + 1, posY).isUsed()) {
			surroundColors[Tile.Side.RIGHT.get()] = Tile.ANY_COLOR;
		} else {
			int rot = getCell(posX + 1, posY).getRotation();
			surroundColors[Tile.Side.RIGHT.get()] = getCell(posX + 1,
					posY).getTile().getRotatedColor(Tile.Side.LEFT, rot);
		}

		return surroundColors;
	}
	
	public EternityTile getColorConstraintsAsDummyTile(int posX, int posY) {
		int topColor = Tile.ANY_COLOR;
		int rightColor = Tile.ANY_COLOR;
		int bottomColor = Tile.ANY_COLOR;
		int leftColor = Tile.ANY_COLOR;
		
		int dimX = this.getDimensionX();
		int dimY = this.getDimensionY();

		// check top
		if (posY == 0) {
			topColor = Tile.BORDER_COLOR;
		} else if (getCell(posX, posY - 1).isUsed()) {
			int rot = getCell(posX, posY - 1).getRotation();
			topColor = getCell(posX,
					posY - 1).getTile().getRotatedColor(Tile.Side.BOTTOM, rot);
		}

		// check left
		if (posX == 0) {
			leftColor = Tile.BORDER_COLOR;
		} else if (getCell(posX - 1, posY).isUsed()) {
			int rot = getCell(posX - 1, posY).getRotation();
			leftColor = getCell(posX - 1,
					posY).getTile().getRotatedColor(Tile.Side.RIGHT, rot);
		}

		// check bottom
		if (posY >= dimY - 1) {
			bottomColor = Tile.BORDER_COLOR;
		} else if (getCell(posX, posY + 1).isUsed()) {
			int rot = getCell(posX, posY + 1).getRotation();
			bottomColor = getCell(posX,
					posY + 1).getTile().getRotatedColor(Tile.Side.TOP, rot);
		}

		// check right
		if (posX >= dimX - 1) {
			rightColor = Tile.BORDER_COLOR;
		} else if (getCell(posX + 1, posY).isUsed()) {
			int rot = getCell(posX + 1, posY).getRotation();
			rightColor = getCell(posX + 1,
					posY).getTile().getRotatedColor(Tile.Side.LEFT, rot);
		}

		return EternityTile.createDummyTile(topColor, rightColor, bottomColor,
				leftColor);
	}
	
	public boolean tileMatchesColors(int posX, int posY, int rotation,
			Tile tile) {
		EternityTile surroundingColors = 
			getColorConstraintsAsDummyTile(posX, posY);
		
		return tile.colorsMatch(surroundingColors, rotation);
	}
	
	public String toString() {
		StringBuilder output = new StringBuilder();
		
		for(int j = 0; j < this.getDimensionY(); j++) {
			for(int i = 0; i < this.getDimensionX(); i++) {
				if(this.getCell(i, j).isUsed()) {
					output.append(this.getTile(i, j).getID());
				} else {
					output.append("-1");
				}
				if(i != this.getDimensionX() - 1) {
					output.append(' ');
				}
			}
			if(j != this.getDimensionY() - 1) {
				output.append('\n');
			}
		}
		
		return output.toString();
	}
}
