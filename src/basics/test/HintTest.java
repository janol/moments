package basics.test;

import static org.junit.Assert.*;

import org.junit.*;

import exceptions.HintException;

import basics.*;

@SuppressWarnings("unused")
public class HintTest {
	Hint h;
	
	@Before
	public void setUp() throws Exception {
		h = new EternityHint(8,9,139,2);
	}

	@Test
	public void testGetID() {
		assertEquals(139, h.getID());
	}

	@Test
	public void testGetRotation() {
		assertEquals(2, h.getRotation());
	}

	@Test
	public void testGetPosX() {
		assertEquals(8, h.getPosX());
	}

	@Test
	public void testGetPosY() {
		assertEquals(9, h.getPosY());
	}
	
	@Test (expected = HintException.class)
	public void testConstruktorBadPosXneg() throws HintException {
		Hint b = new EternityHint(-1,9,139,2);
	}
	
	@Test (expected = HintException.class)
	public void testConstruktorBadPosXpos() throws HintException {
		Hint b = new EternityHint(22,9,139,2);
	}
	
	@Test (expected = HintException.class)
	public void testConstruktorBadPosYneg() throws HintException {
		Hint b = new EternityHint(8,-1,139,2);
	}
	
	@Test (expected = HintException.class)
	public void testConstruktorBadPosYpos() throws HintException {
		Hint b = new EternityHint(8,22,139,2);
	}

	@Test (expected = HintException.class)
	public void testConstruktorBadIdneg() throws HintException {
		Hint b = new EternityHint(8,9,0,2);
	}
	
	@Test (expected = HintException.class)
	public void testConstruktorBadIdpos() throws HintException {
		Hint b = new EternityHint(8,9,300,2);
	}
	
	@Test (expected = HintException.class)
	public void testConstruktorBadRotneg() throws HintException {
		Hint b = new EternityHint(8,9,139,-1);
	}
	
	@Test (expected = HintException.class)
	public void testConstruktorBadRotpos() throws HintException {
		Hint b = new EternityHint(8,9,139,4);
	}
}
