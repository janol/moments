package basics.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exceptions.PlacingException;

import basics.CheckedBoard;
import basics.EternityBoard;
import basics.EternityTile;
import basics.Tile;

public class CheckedBoardTest {
	CheckedBoard board;
	Tile[] tiles;
	
	@Before
	public void setUp() throws Exception {
		board = new CheckedBoard(new EternityBoard(3,3));
		tiles = new Tile[2];
		tiles[0] = new EternityTile(0, 0, 2, 3, 0);
		tiles[1] = new EternityTile(1, 0, 3, 4, 2);
		
		board.placeTile(0, 0, 0, tiles[0]);
	}

	@Test(expected = PlacingException.class)
	public void testPlaceTile() throws PlacingException {
		board.placeTile(0, 1, 0, tiles[1]);
	}

	@Test(expected = PlacingException.class)
	public void testCheckedBoard() throws PlacingException {
		EternityBoard badBoard = new EternityBoard(3, 3);
		
		badBoard.placeTile(0, 0, 0, tiles[0]);
		badBoard.placeTile(0, 1, 0, tiles[1]);
		
		@SuppressWarnings("unused")
		CheckedBoard cantBeBuilt = new CheckedBoard(badBoard);
	}

	@Test
	public void testTileMatchesColors() {
		assertTrue(board.tileMatchesColors(1, 0, 0, tiles[1]));
	}

	@Test
	public void testGetNumberOfTilesPlaced() {
		assertEquals(1, board.getNumberOfPlacedTiles());
	}
	
	@Test
	public void testRemoveTile() {
		assertTrue(board.getCell(0, 0).isUsed());
		
		board.removeTile(0, 0);
		assertFalse(board.getCell(0, 0).isUsed());
	}
	
	@Test
	public void testRemoveTileNull() {
		assertEquals(null, board.removeTile(1, 1));
	}
	
	@Test
	public void testGetMatchingRotations() {
		assertEquals(1, board.getMatchingRotations(1, 0, tiles[1]).size());
	}
	
	@Test
	public void testGetMatchingRotationsWithRotationFits() {
		assertEquals(1, board.getMatchingRotations(1, 0, 0, tiles[1]).size());
	}
	
	@Test
	public void testGetMatchingRotationsWithRotationFails() {
		assertEquals(0, board.getMatchingRotations(1, 0, 1, tiles[1]).size());
	}
}
