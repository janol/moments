package basics.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import basics.*;
import basics.Tile.Side;

public class EternityTileTest {
	Tile t;
	Tile dummyTile;
	
	@Before
	public void setUp() throws Exception {
		t = new EternityTile(200,1,2,3,4);
		dummyTile = EternityTile.createDummyTile(Tile.ANY_COLOR, 1,
				Tile.ANY_COLOR, Tile.ANY_COLOR);
	}

	@Test
	public void testGetID() {
		assertEquals(200, t.getID());
	}

	@Test
	public void testGetColorTop() {
		assertEquals(1, t.getColor(Side.TOP));
	}
	
	@Test
	public void testGetColorRight() {
		assertEquals(2, t.getColor(Side.RIGHT));
	}
	
	@Test
	public void testGetColorBottom() {
		assertEquals(3, t.getColor(Side.BOTTOM));
	}
	
	@Test
	public void testGetColorLeft() {
		assertEquals(4, t.getColor(Side.LEFT));
	}

	@Test
	public void testGetRotatedColor() {
		assertEquals(4, t.getRotatedColor(Side.TOP, 1));
	}
	
	@Test
	public void testIsDummy() {
		assertFalse(t.isDummyTile());
		assertTrue(dummyTile.isDummyTile());
	}
	
	@Test
	public void testColorsMatch() {
		assertTrue(t.colorsMatch(dummyTile, 1));
		assertTrue(dummyTile.colorsMatch(t, 3));
	}
	
	@Test
	public void testColorMatchFalse() {
		Tile fTile = new EternityTile(0, 25, 25, 25, 25);
		assertFalse(t.colorsMatch(fTile, 0));
	}
}
