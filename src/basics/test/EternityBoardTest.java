package basics.test;

import static org.junit.Assert.*;
import basics.*;
import basics.Tile.Side;

import org.junit.Before;
import org.junit.Test;

import exceptions.PlacingException;

public class EternityBoardTest {
	Board b;
	Tile t;
	Tile[] tiles;
	@Before
	public void setUp() throws Exception {
		b = new EternityBoard(16,16);
		t= new EternityTile(0,1,2,3,4);

		tiles = new Tile[4];
		for (int i = 0; i < tiles.length; i++) {
			tiles[i] = new EternityTile(i,1,i*2,3,i+2);
		}
	}

	@Test(expected = PlacingException.class)
	public void testPlaceTileBadXneg() throws PlacingException{
		b.placeTile(-1, 0, 0, t);
	}
	
	@Test(expected = PlacingException.class)
	public void testPlaceTileBadYneg() throws PlacingException{
		b.placeTile(0, -1, 0, t);
	}
	
	@Test(expected = PlacingException.class)
	public void testPlaceTileBadRot() throws PlacingException{
		b.placeTile(0, 0, 45, t);
	}
	
	@Test(expected = PlacingException.class)
	public void testPlaceTileBadXpos() throws PlacingException{
		b.placeTile(16, 0, 0, t);
	}
	
	@Test(expected = PlacingException.class)
	public void testPlaceTileBadYpos() throws PlacingException{
		b.placeTile(0, 16, 0, t);
	}
	
	@Test(expected = NullPointerException.class)
	public void testPlaceTileBadTile() throws PlacingException{
		b.placeTile(0, 0, 0, null);
	}
	
	@Test
	public void testGetTile() throws PlacingException{
		b.placeTile(0, 0, 0, t);
		assertEquals(t, b.getTile(0, 0));
	}
	
	@Test
	public void testGetRotation() throws PlacingException{
		b.placeTile(0, 0, 3, t);
		assertEquals(3, b.getRotation(0, 0));
	}
	
	@Test
	public void testKnownStatus(){
		b.setKnown(5, 5, true);
		assertTrue(b.isKnown(5, 5));
	}
	
	@Test
	public void testDimensions(){
		assertEquals(16, b.getDimensionX());
		assertEquals(16, b.getDimensionY());
	}
	
	@Test
	public void testEquals() throws PlacingException{
		Board board1 = new EternityBoard(2,2);
		Board board2 = new EternityBoard(2,2);
		
		board1.placeTile(0, 0, 0, tiles[0]);
		board1.placeTile(0, 1, 1, tiles[1]);
		board1.placeTile(1, 0, 2, tiles[2]);
		board1.placeTile(1, 1, 3, tiles[3]);
		
		board2.placeTile(0, 0, 0, tiles[0]);
		board2.placeTile(0, 1, 1, tiles[1]);
		board2.placeTile(1, 0, 2, tiles[2]);
		board2.placeTile(1, 1, 3, tiles[3]);
		
		assertTrue(board1.equals(board2));
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetSurroundColorsTopLeft() {
		Board board1 = new EternityBoard(2, 2);
		
		int[] colors = board1.getSurroundingColors(0, 0);
		int[] testColors = new int[]{0, -1, -1, 0};
		
		for (int i = 0; i < 4; i++) {
			if (colors[i] != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetSurroundColorsTopRight() {
		Board board1 = new EternityBoard(2, 2);
		
		int[] colors = board1.getSurroundingColors(1, 0);
		int[] testColors = new int[]{0, 0, -1, -1};
		
		for (int i = 0; i < 4; i++) {
			if (colors[i] != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetSurroundColorsBottomRight() {
		Board board1 = new EternityBoard(2, 2);
		
		int[] colors = board1.getSurroundingColors(1, 1);
		int[] testColors = new int[]{-1, 0, 0, -1};
		
		for (int i = 0; i < 4; i++) {
			if (colors[i] != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetSurroundColorsBottomLeft() {
		Board board1 = new EternityBoard(2, 2);
		
		int[] colors = board1.getSurroundingColors(0, 1);
		int[] testColors = new int[]{-1, -1, 0, 0};
		
		for (int i = 0; i < 4; i++) {
			if (colors[i] != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetSurroundColorsCenter() throws PlacingException {
		//Top
		b.placeTile(12, 12, 0, t);
		//Right
		b.placeTile(13, 13, 0, t);
		//Bottom
		b.placeTile(12, 14, 0, t);
		//Left
		b.placeTile(11, 13, 0, t);
		
		int[] colors = b.getSurroundingColors(12, 13);
		int[] testColors = new int[]{3, 4, 1, 2};
		
		for (int i = 0; i < 4; i++) {
			if (colors[i] != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@Test
	public void testGetColorConstraintsAsDummyTileTopLeft() {
		Board board1 = new EternityBoard(2, 2);
		
		Tile colors = board1.getColorConstraintsAsDummyTile(0, 0);
		int[] testColors = new int[]{0, -1, -1, 0};
		
		for (int i = 0; i < 4; i++) {
			if (colors.getColor(Side.values()[i]) != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@Test
	public void testGetColorConstraintsAsDummyTileTopRight() {
		Board board1 = new EternityBoard(2, 2);
		
		Tile colors = board1.getColorConstraintsAsDummyTile(1, 0);
		int[] testColors = new int[]{0, 0, -1, -1};
		
		for (int i = 0; i < 4; i++) {
			if (colors.getColor(Side.values()[i]) != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@Test
	public void testGetColorConstraintsAsDummyTileBottomRight() {
		Board board1 = new EternityBoard(2, 2);
		
		Tile colors = board1.getColorConstraintsAsDummyTile(1, 1);
		int[] testColors = new int[]{-1, 0, 0, -1};
		
		for (int i = 0; i < 4; i++) {
			if (colors.getColor(Side.values()[i]) != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@Test
	public void testGetColorConstraintsAsDummyTileBottomLeft() {
		Board board1 = new EternityBoard(2, 2);
		
		Tile colors = board1.getColorConstraintsAsDummyTile(0, 1);
		int[] testColors = new int[]{-1, -1, 0, 0};
		
		for (int i = 0; i < 4; i++) {
			if (colors.getColor(Side.values()[i]) != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@Test
	public void testGetColorConstraintsAsDummyTileCenter() throws PlacingException {
		//Top
		b.placeTile(12, 12, 0, t);
		//Right
		b.placeTile(13, 13, 0, t);
		//Bottom
		b.placeTile(12, 14, 0, t);
		//Left
		b.placeTile(11, 13, 0, t);
		
		Tile colors = b.getColorConstraintsAsDummyTile(12, 13);
		int[] testColors = new int[]{3, 4, 1, 2};
		
		for (int i = 0; i < 4; i++) {
			if (colors.getColor(Side.values()[i]) != testColors[i]) {
				fail("Assertion Fails due to wrong returnColors");
			}
		}
	}
	
	@Test
	public void testTileMatchesColors() {
		Tile t1 = new EternityTile(0, 0, 0, 0, 0);
		assertTrue(b.tileMatchesColors(0, 0, 0, t1));
	}
	
	@Test
	public void testTileMatchesColorsFail() {
		Tile t1 = new EternityTile(0, 2, 0, 0, 0);
		assertFalse(b.tileMatchesColors(0, 0, 0, t1));
	}
	
	@Test
	public void testEqualsNoBoard() {
		assertFalse(b.equals(new Boolean(false)));
	}
	
	@Test
	public void testEqualsWrongDim() {
		assertFalse(b.equals(new EternityBoard(3, 3)));
	}
	
	@Test
	public void testWrongBoard() throws PlacingException {
		Board board1 = new EternityBoard(2,2);
		Board board2 = new EternityBoard(2,2);
		
		board1.placeTile(0, 0, 0, tiles[0]);
		board1.placeTile(0, 1, 1, tiles[1]);
		board1.placeTile(1, 0, 2, tiles[2]);
		board1.placeTile(1, 1, 3, tiles[3]);
		
		board2.placeTile(0, 0, 0, tiles[0]);
		board2.placeTile(0, 1, 1, tiles[1]);
		board2.placeTile(1, 0, 2, tiles[2]);
		//here is the rotation off
		board2.placeTile(1, 1, 0, tiles[3]);
		
		assertFalse(board1.equals(board2));
	}
	
	@Test
	public void testNullPosition() throws PlacingException {
		Board board1 = new EternityBoard(2,2);
		Board board2 = new EternityBoard(2,2);
		
		board1.placeTile(0, 0, 0, tiles[0]);
		board1.placeTile(0, 1, 1, tiles[1]);
		board1.placeTile(1, 0, 2, tiles[2]);
		
		board2.placeTile(0, 0, 0, tiles[0]);
		board2.placeTile(0, 1, 1, tiles[1]);
		board2.placeTile(1, 0, 2, tiles[2]);
		
		assertTrue(board1.equals(board2));
	}
}
