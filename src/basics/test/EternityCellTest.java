package basics.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exceptions.*;

import basics.*;

public class EternityCellTest {
	Cell c;
	Tile t;
	
	@Before
	public void setUp() throws Exception {
		c = new EternityCell();
		t = new EternityTile(100,2,3,4,5);
	}

	@Test
	public void testGetTile() {
		c.setTile(t);
		assertEquals(t, c.getTile());
	}

	@Test
	public void testGetRotation() throws PlacingException {
		c.setRotation(2);
		assertEquals(2, c.getRotation());
	}

	@Test (expected = NullPointerException.class)
	public void testSetTileBad() {
		c.setTile(null);
	}

	@Test (expected = PlacingException.class)
	public void testSetRotationBadneg() throws PlacingException{
		c.setRotation(-1);
	}
	
	@Test (expected = PlacingException.class)
	public void testSetRotationBadpos() throws PlacingException{
		c.setRotation(44);
	}

	@Test
	public void testGetKnown(){
		c.setKnown(true);
		assertTrue(c.isKnown());
	}
	
	@Test
	public void testIsUsedNot() {
		assertFalse(c.isUsed());
	}
	
	@Test
	public void testIsUsed() {
		c.setTile(t);
		assertTrue(c.isUsed());
	}
	
	@Test
	public void testResetCell() {
		c.setTile(t);
		
		c.resetCell();
		assertFalse(c.isUsed());
	}
}
