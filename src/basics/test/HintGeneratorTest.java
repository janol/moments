package basics.test;


import java.io.File;
import java.util.*;

import static org.junit.Assert.*;
import org.junit.*;

import basics.*;
import basics.Tile.Side;
import exceptions.HintException;
import io.EternityTileReader;

public class HintGeneratorTest {
	private Tile[] tiles = null;
	private Hint[] hints = null;
	private EternityTileReader TReader = new EternityTileReader();

	@Before
	public void setUp() {
		try {
			tiles = TReader.getTiles("resources" + File.separator + "tiles4x4.txt");
			hints = TReader.getHints("resources" + File.separator + "hints4x4.txt");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGenerateProblemSetsCount() {
		assertEquals(24, HintGenerator.generateProblemSets(tiles, hints, 4, 4)
				.size());
	}

	@Test
	public void testGenerateProblemSets() {
		ArrayList<Hint[]> problemSets = HintGenerator.generateProblemSets(
				tiles, hints, 4, 4);
		assertFalse(this.checkDouble(problemSets));
	}

	@Test
	public void testDefineCornersSize() {
		ArrayList<Integer[]> corners = HintGenerator.defineCorners(4, 4);
		assertEquals(4, corners.size());
	}

	@Test
	public void testDefineCorners() {
		ArrayList<Integer[]> corners = HintGenerator.defineCorners(4, 4);
		assertTrue(containsAllPositions(corners));
	}

	// counts the cornerpieces
	private boolean containsAllPositions(ArrayList<Integer[]> corners) {
		int count = 0;
		ArrayList<Integer[]> c = new ArrayList<Integer[]>();
		c.add(new Integer[] { 0, 3 });
		c.add(new Integer[] { 0, 0 });
		c.add(new Integer[] { 3, 3 });
		c.add(new Integer[] { 3, 0 });
		for (Integer[] corner : corners) {
			for (Integer[] pos : c) {
				if ((pos[0] == corner[0]) && (pos[1] == corner[1])) {
					count++;
				}
			}
		}
		return count == 4;
	}

	@Test
	public void testGetCornersCount() {
		assertEquals(4, HintGenerator.getCorners(tiles).size());
	}

	@Test
	public void testGetCorners() {
		assertTrue(checkCorners(HintGenerator.getCorners(tiles)));
	}

	// check whether all corners (in this case 1-4) are returned
	private boolean checkCorners(ArrayList<Tile> corners) {
		boolean[] eCorner = new boolean[4];
		Arrays.fill(eCorner, false);
		boolean pass = true;
		for (Tile t : corners) {
			eCorner[t.getID() - 1] = true;
		}

		for (Boolean b : eCorner) {
			pass &= b;
		}
		return pass;
	}

	@Test
	public void testCheckHintCornersCount() {
		ArrayList<Integer[]> pos = HintGenerator.defineCorners(4, 4);
		ArrayList<Tile> con = HintGenerator.getCorners(tiles);
		HintGenerator.checkHintCorners(pos, con, hints);
		assertEquals(4, con.size());
		assertEquals(4, pos.size());
	}

	@Test
	public void testCheckHintCorners() throws HintException {
		ArrayList<Integer[]> pos = HintGenerator.defineCorners(4, 4);
		ArrayList<Tile> con = HintGenerator.getCorners(tiles);
		HintGenerator.checkHintCorners(pos, con, new Hint[] { new EternityHint(
				0, 0, 1, 0) });
		assertTrue(checkCornersAndTiles(con, pos));
	}

	private boolean checkCornersAndTiles(ArrayList<Tile> con,
			ArrayList<Integer[]> pos) {
		boolean equal = true;
		ArrayList<Integer[]> positionCheck = new ArrayList<Integer[]>();
		ArrayList<Tile> tileCheck = new ArrayList<Tile>();
		positionCheck.add(new Integer[] { 0, 3 });
		positionCheck.add(new Integer[] { 3, 0 });
		positionCheck.add(new Integer[] { 3, 3 });

		tileCheck.add(tiles[1]);
		tileCheck.add(tiles[2]);
		tileCheck.add(tiles[3]);

		for (int i = 0; i < tileCheck.size(); i++) {
			equal = con.contains(tileCheck.get(i));
		}

		for (int i = 0; i < positionCheck.size(); i++) {
			Integer[] posC = positionCheck.get(i);
			for (int j = 0; j < pos.size(); j++){
				Integer[] posG = positionCheck.get(j);
				int count = 0;
				
				if ((posC[0] != posG[0]) || (posC[1] != posG[1])){
					count++;
					if (count == 3){
						equal = false;
					}
				}
			}
		}
		return equal;
	}

	@Test
	public void testGetCornerProblemSetsCount() {
		ArrayList<Tile> corners = HintGenerator.getCorners(tiles);
		ArrayList<Choice> basicCorners = HintGenerator
				.getRotatedCornerTiles(corners);
		ArrayList<Integer[]> emptyCorners = HintGenerator.defineCorners(4, 4);
		ArrayList<Hint[]> problemSet = new ArrayList<Hint[]>();
		HintGenerator.getCornerProblemSets(basicCorners, emptyCorners,
				new ArrayList<Hint>(), problemSet);
		assertEquals(24, problemSet.size());
	}

	@Test
	public void testGetCornerProblemSets() {
		ArrayList<Tile> corners = HintGenerator.getCorners(tiles);
		ArrayList<Choice> basicCorners = HintGenerator
				.getRotatedCornerTiles(corners);
		ArrayList<Integer[]> emptyCorners = HintGenerator.defineCorners(4, 4);
		ArrayList<Hint[]> problemSet = new ArrayList<Hint[]>();
		HintGenerator.getCornerProblemSets(basicCorners, emptyCorners,
				new ArrayList<Hint>(), problemSet);
		assertFalse(checkDouble(problemSet));
	}

	// returns true if there are 2 equal sets of hints
	private boolean checkDouble(ArrayList<Hint[]> problemSet) {
		boolean equals = false;
		for (int i = 0; i < problemSet.size() - 1; i++) {
			for (int j = i + 1; j < problemSet.size(); j++) {
				int count = 0;
				Hint[] hintSet1 = problemSet.get(i);
				Hint[] hintSet2 = problemSet.get(j);

				for (Hint h1 : hintSet1) {
					for (Hint h2 : hintSet2) {
						if (h1.equals(h2)) {
							count++;
						}
					}
				}

				if (count >= hintSet1.length) {
					equals = true;
				}
			}
		}
		return equals;
	}

	@Test
	public void testGetRotatedCornersCount() {
		ArrayList<Tile> corners = HintGenerator.getCorners(tiles);
		ArrayList<Choice> basicCorners = HintGenerator
				.getRotatedCornerTiles(corners);
		assertEquals(corners.size(), basicCorners.size());
	}

	@Test
	public void testGetRotatedCorners() {
		ArrayList<Tile> con = HintGenerator.getCorners(tiles);
		ArrayList<Choice> cCon = HintGenerator.getRotatedCornerTiles(con);
		for (Choice c : cCon) {
			if (tiles[c.getID() - 1].getRotatedColor(Side.TOP, c.getRotation()) != 0
					|| tiles[c.getID() - 1].getRotatedColor(Side.RIGHT, c
							.getRotation()) != 0) {
				fail("not the right Rotation");
			}
		}
	}
}
