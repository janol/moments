package basics.test;

import static org.junit.Assert.*;

import org.junit.*;

import basics.*;

public class EternityChoiceTest {
	Choice c;
	
	@Before
	public void setUp(){
		c = new EternityChoice(new EternityTile(222, 1, 2, 3, 4), 2);
	}
	
	@Test
	public void testGetID() {
		assertEquals(222, c.getID());
	}

	@Test
	public void testGetRotation() {
		assertEquals(2, c.getRotation());
	}

}
