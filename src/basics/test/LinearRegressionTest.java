package basics.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import basics.LinearRegression;

public class LinearRegressionTest {

	private ArrayList<Long> testValues;
	
	@Before
	public void setUp() {
		this.testValues = new ArrayList<Long>();
		this.testValues.add(3l);
		this.testValues.add(7l);
		this.testValues.add(20l);
		this.testValues.add(55l);
	}
	
	@Test
	public void testGetNextTime() {
		long l = LinearRegression.getNextTime(this.testValues);
		assertEquals(148, l, 10);
	}

}
