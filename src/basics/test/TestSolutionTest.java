package basics.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import io.EternityTileReader;

import org.junit.Before;
import org.junit.Test;

import exceptions.EmptyFileException;
import exceptions.NoFileException;
import exceptions.PlacingException;

import algorithm.BacktrackAlgorithm;
import basics.Board;
import basics.EternityBoard;
import basics.EternityTile;
import basics.Hint;
import basics.TestSolution;
import basics.Tile;


public class TestSolutionTest {
	public Tile[] tiles;
	public Board board;
	
	@Before
	public void setUp() throws IOException, EmptyFileException, NoFileException {
		tiles = new EternityTileReader().getTiles("resources" + File.separator + "tiles2x2.txt");
		board = new EternityBoard(2, 2);
	}
	
	@Test
	public void testNoTileSet() {
		assertFalse(TestSolution.isSolution(board));
	}
	
	@Test 
	public void testWrongTileCombination() throws PlacingException {
		board.placeTile(0, 0, 0, tiles[0]);
		board.placeTile(0, 1, 0, tiles[1]);
		board.placeTile(1, 0, 0, tiles[2]);
		board.placeTile(1, 1, 0, tiles[3]);
		
		assertFalse(TestSolution.isSolution(board));
	}
	
	@Test 
	public void testRightBoard() throws PlacingException {
		BacktrackAlgorithm algo = new BacktrackAlgorithm(board, tiles, new Hint[0]);
		
		for (int i = 0; i < 4; i++) {
			algo.calculateStep();
		}
		assertTrue(TestSolution.isSolution(algo.getBoard()));
	}
	
	@Test 
	public void testRightPosition() throws PlacingException {
		BacktrackAlgorithm algo = new BacktrackAlgorithm(board, tiles, new Hint[0]);
		
		for (int i = 0; i < 4; i++) {
			algo.calculateStep();
		}
		assertTrue(TestSolution.fitPiece(algo.getBoard(), 0, 0));
	}
	
	@Test 
	public void testWrongPosition() throws PlacingException {
		board.placeTile(0, 0, 0, tiles[0]);
		board.placeTile(0, 1, 0, tiles[1]);
		board.placeTile(1, 0, 0, tiles[2]);
		board.placeTile(1, 1, 0, tiles[3]);
		
		assertFalse(TestSolution.fitPiece(board, 0, 0));
	}
	
	@Test
	public void testCenterPositionFail() throws PlacingException {
		Board b = new EternityBoard(3, 3);
		
		b.placeTile(1, 0, 0, tiles[0]);
		b.placeTile(1, 2, 0, tiles[1]);
		b.placeTile(0, 1, 0, tiles[2]);
		b.placeTile(2, 1, 0, tiles[3]);
		b.placeTile(1, 1, 0, tiles[0]);
		
		assertFalse(TestSolution.fitPiece(b, 1, 1));
	}
	
	@Test
	public void testCenterPositionPass() throws PlacingException {
		Board b = new EternityBoard(3, 3);
		
		b.placeTile(1, 0, 0, tiles[0]);
		b.placeTile(1, 2, 0, tiles[1]);
		b.placeTile(0, 1, 0, tiles[2]);
		b.placeTile(2, 1, 0, tiles[3]);
		b.placeTile(1, 1, 0, new EternityTile(0, 1, 3, 0, 0));
		
		assertTrue(TestSolution.fitPiece(b, 1, 1));
	}
}
