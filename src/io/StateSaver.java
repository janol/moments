package io;

import java.io.*;

/**
 * Kept for now; might be rewritten in the future with
 * exact types
 * @author OLJA100
 *
 */
public class StateSaver {
	/**
	 * No public constructor as this is a sole utility class
	 */
	private StateSaver() {		
	}

	public static Object getObject(String file) {
		InputStream fio = null; 
		Object state = null;
		 
		try 
		{ 
		  fio = new FileInputStream(file); 
		  ObjectInputStream o = new ObjectInputStream( fio ); 
		  
		 state = o.readObject();
		} 
		catch (IOException e){ 
			System.err.println( e );
		}
		catch (ClassNotFoundException e){ 
			System.err.println( e ); 
		}
		finally { 
			try { 
				fio.close(); 
			} catch (Exception e){} 
		}
		return state;
	}

	public static boolean saveObject(Object state, String file) {
		OutputStream fos = null; 
		
		try 
		{ 
		  fos = new FileOutputStream(file); 
		  ObjectOutputStream o = new ObjectOutputStream( fos ); 
		  o.writeObject(state); 
		} 
		catch ( IOException e ){ 
			System.err.println( e );
			return false;
		} 
		finally{ 
			try{ 
				fos.close(); 
			} 
			catch (Exception e){
				return false;
			}
		}
		return true;
	}

}
