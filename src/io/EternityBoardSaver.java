package io;

import java.io.*;

import basics.Board;

/**
 * Contains a function to savel the Board into file top left is position(0,0)
 * 
 * @author OLJA100
 * 
 */
public class EternityBoardSaver {

	/**
	 * writes the Board to file starts top left as (0,0)
	 * 
	 * @param board
	 * @param file
	 */
	public static void saveBoard(Board board, String file) {
		Writer fw = null;
		int dimX = board.getDimensionX();
		int dimY = board.getDimensionY();

		try {
			//Open File
			fw = new FileWriter(file);
			for (int i = 0; i < dimY; i++) {
				for (int j = 0; j < dimX; j++) {
					// check for size of ID to make sure the board is formatted
					if (board.getTile(j, i) == null) {
						fw.write("  -1   ");
					} else if (board.getTile(j, i).getID() > 99) {
						fw.write(board.getTile(j, i).getID() + "r"
								+ board.getRotation(j, i));
					} else if (board.getTile(j, i).getID() > 9) {
						fw.write(" " + board.getTile(j, i).getID() + "r"
								+ board.getRotation(j, i));
					} else {
						fw.write(" " + board.getTile(j, i).getID() + "r"
								+ board.getRotation(j, i) + " ");
					}
					fw.write("  ");
				}
				fw.append(System.getProperty("line.separator")); // e.g. "\n"
			}
		} catch (IOException e) {
			System.err.println("Konnte Datei nicht erstellen");
		} finally {
			if (fw != null){
				try {
					fw.close();
				} catch (IOException e) {
				}
			}	
		}
	}
}
