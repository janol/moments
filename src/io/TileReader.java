package io;

import java.io.IOException;
import java.io.Serializable;

import basics.Hint;
import basics.Tile;

import exceptions.EmptyFileException;
import exceptions.HintException;
import exceptions.NoFileException;

public abstract class TileReader implements Serializable{
	private static final long serialVersionUID = 7391797548802618956L;

	/**
	 * get Tiles from File
	 * Tile in File -> ID, Top, Right, Bottom, Left
	 * @param file
	 * @return array of Tiles
	 * @throws IOException 
	 * @throws NoFileException 
	 */
	public abstract Tile[] getTiles(String file) throws IOException, EmptyFileException, NoFileException;
	
	/**
	 * get Tiles and check whether dimensions fit with tiles count
	 * @param file
	 * @param dimX
	 * @param dimY
	 * @return
	 * @throws IOException
	 * @throws EmptyFileException
	 * @throws NoFileException
	 */
	public abstract Tile[] getTiles(String file, int dimX, int dimY) throws IOException, EmptyFileException, NoFileException;
	
	/**
	 * get Hints from File
	 * Hint in File -> PosX, PosY, ID, Rotation
	 * @param File
	 * @return array of Hints
	 * @throws IOException 
	 * @throws HintException 
	 * @throws NumberFormatException 
	 * @throws NoFileException 
	 */
	public abstract Hint[] getHints(String File) throws IOException, NumberFormatException, HintException, NoFileException;
}
