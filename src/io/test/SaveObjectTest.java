package io.test;


import static org.junit.Assert.*;
import io.StateSaver;

import org.junit.*;

import basics.*;

public class SaveObjectTest {
	EternityBoard field2;
	
	@Before
	public void setUp() throws Exception {
		EternityBoard field = new EternityBoard(16,16);
		field.placeTile(5, 5, 2, new EternityTile(139,1,3,4,5));
		StateSaver.saveObject(field, "test.src");
		
		field2=null;
		
		field2 = (EternityBoard) StateSaver.getObject("test.src");
	}
	
	@Test
	public void testFieldId(){
		assertEquals(139, field2.getTile(5, 5).getID());
	}
}
