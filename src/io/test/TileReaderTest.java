package io.test;


import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

import org.junit.*;

import io.EternityTileReader;
import basics.*;
import basics.Tile.Side;
import exceptions.*;

public class TileReaderTest {
	Tile[] Tiles;
	Hint[] Hints;
	EternityTileReader TReader;
	
	@Before
	public void setUp() throws Exception {
		TReader = new EternityTileReader();
		Tiles = null;
		Tiles = TReader.getTiles("resources" + File.separator + "tiles4x4.txt");
		Hints = null;
		Hints = TReader.getHints("resources" + File.separator + "testhints4x4.txt");
	}

	@Test
	public void testGetTilesLength(){
		assertEquals(16, Tiles.length);
	}
	
	@Test
	public void testGetTiles1(){
		assertEquals(0, Tiles[0].getColor(Side.TOP));
	}
	
	@Test
	public void testGetTiles2(){
		assertEquals(1, Tiles[2].getColor(Side.TOP));
	}
	
	@Test
	public void testGetHintsLength(){
		assertEquals(1, Hints.length);
	}
	
	@Test
	public void testGetHints1(){
		assertEquals(139, Hints[0].getID());
	}
	
	@Ignore @Test
	public void testGetHints2(){
		assertEquals(181, Hints[1].getID());
	}
	
	@Test (expected = EmptyFileException.class)
	public void testEmptyFile() throws EmptyFileException, Exception {
		Tiles = TReader.getTiles("resources" + File.separator + "emptyTiles.txt");
	}
	
	@Test (expected = NoFileException.class)
	public void testHintNoFile() throws NumberFormatException, IOException, HintException, NoFileException {
		Hints = TReader.getHints("resources" + File.separator + "tex4.txt");
	}
	
	@Test (expected = NoFileException.class)
	public void testFileNoFile() throws NumberFormatException, IOException, HintException, NoFileException, EmptyFileException {
		Tiles = TReader.getTiles("resources" + File.separator + "til4.txt");
	}
	
	@Test (expected = EmptyFileException.class)
	public void testWrongDimensions() throws IOException, EmptyFileException, NoFileException {
		Tiles = TReader.getTiles("resources" + File.separator + "tiles4x4.txt", 2, 2);
	}
}
