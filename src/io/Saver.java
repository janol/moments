package io;

public abstract class Saver {
	
	/**
	 * reads Object from File
	 * @param file
	 * @return
	 */
	public abstract Object getObject(String file);
	
	/**
	 * saves Object to File
	 * @param state
	 * @param file
	 */
	public abstract boolean saveObject(Object state, String file);
}
