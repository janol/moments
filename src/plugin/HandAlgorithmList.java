package plugin;

import java.util.ArrayList;

public class HandAlgorithmList extends AlgorithmListPlugin {
	
	private ArrayList<String> algos;
	
	public HandAlgorithmList() {
		this.algos = new ArrayList<String>();
		algos.add("algorithm.BacktrackAlgorithm");
		algos.add("algorithm.SimpleBacktracker");
	}
	
	@Override
	public ArrayList<String> getAlgorithmList() {
		return this.algos;
	}

}
