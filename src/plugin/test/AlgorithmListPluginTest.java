package plugin.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import plugin.AlgorithmListPlugin;
import plugin.HandAlgorithmList;

public class AlgorithmListPluginTest {

	private AlgorithmListPlugin plugin;
	private ArrayList<String> algos;
	
	@Before
	public void setUp() {
		this.plugin = new HandAlgorithmList();
		algos = new ArrayList<String>();
		algos.add("algorithm.BacktrackAlgorithm");
		algos.add("algorithm.SimpleBacktracker");
	}
	
	@Test
	public void testGetAlgorithmList() {
		assertTrue(algos.equals(plugin.getAlgorithmList()));
	}

}
