package plugin;

import java.util.ArrayList;

public abstract class AlgorithmListPlugin {
	/**
	 * returns List of available algorithms
	 * @return
	 */
	public abstract ArrayList<String> getAlgorithmList();
}
