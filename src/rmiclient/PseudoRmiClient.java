package rmiclient;


import io.EternityTileReader;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import exceptions.EmptyFileException;
import exceptions.NoFileException;

import basics.EternityHint;
import basics.Log4jConfigurator;
import basics.Tile;

import rmiserver.*;


public class PseudoRmiClient {
	
	private Server stub;
	private String algorithmType = "algorithm.BacktrackAlgorithm";
	private EternityTileReader eternityReader = null;
	private Tile[] eternityTiles;
	private int safeTime = 6;
	
	public PseudoRmiClient(String[] args) throws IOException, EmptyFileException {
		String host = (args.length < 1) ? null : args[0];
		try {
		    Registry registry = LocateRegistry.getRegistry(host);
		    stub = (Server) registry.lookup("eternity");
		} catch (Exception e) {
		    System.err.println("Client exception: " + e.toString());
		    e.printStackTrace();
		}
		
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
		
	}
	
	public RunningStates getStatus() throws RemoteException{
		return stub.getStatus();
		
	}
	
	public void startCalculation() throws IOException, EmptyFileException{
		this.eternityReader = new EternityTileReader();
		try {
			this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles10x10fast.txt");
		} catch (NoFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		stub.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 10, 10, safeTime);
	}
	
	public void stopCalculation() throws RemoteException{
		stub.stopCalculation();
	}
	
	public static void main(String[] args) {

		try {
			PseudoRmiClient rc = new PseudoRmiClient(args);
			//rc.startCalculation();
			rc.stopCalculation();
			switch (rc.getStatus()){
				case INACTIVE : System.out.println("Inactive");
								break;
				case RUNNING : System.out.println("Running");
								break;
				case TERMINATED : System.out.println("Terminated");
								break;
				default : System.out.println("wrong entry");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EmptyFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
