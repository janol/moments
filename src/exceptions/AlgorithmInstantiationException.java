package exceptions;

public class AlgorithmInstantiationException extends Exception {
	private static final long serialVersionUID = -6593865224479055216L;

	public AlgorithmInstantiationException() {
	}

	public AlgorithmInstantiationException(String arg0) {
		super(arg0);
	}

	public AlgorithmInstantiationException(Throwable arg0) {
		super(arg0);
	}

	public AlgorithmInstantiationException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}
}
