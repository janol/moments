package exceptions;

@SuppressWarnings("serial")
public class PlacingException extends Exception {

	public PlacingException() {
		super();
	}

	public PlacingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public PlacingException(String arg0) {
		super(arg0);
	}

	public PlacingException(Throwable arg0) {
		super(arg0);
	}	
}
