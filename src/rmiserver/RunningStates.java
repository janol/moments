package rmiserver;

public enum RunningStates {
	INACTIVE,	//no algorithm started and also calculation stopped
	RUNNING,	//crunching down tiles
	TERMINATED	//solution or no solution found
}
