package rmiserver;

import java.rmi.*;
import java.util.*;

import threading.AlgorithmThread;

import basics.*;

public interface Server extends Remote {
	
	/**
	 * returns status of server:
	 * running, finished, inactive
	 * @return
	 */
	RunningStates getStatus() throws RemoteException;
	
	/**
	 * returns board if solution found
	 * in case there is no solution return value is null
	 * @return
	 */
	Board getSolution() throws RemoteException;
	
	/**
	 * starts calculation 
	 * defines algoritmtype, boardsize, tiledefinitions and if there is a backup..
	 * @param algorithmType
	 * @param tiles
	 * @param hints
	 * @param dimX
	 * @param dimY
	 * @param backup
	 */
	void startCalculation(String algorithmType, Tile[] tiles,
			Hint[] hints, int dimX, int dimY, int safeTime) throws RemoteException;
	
	/**
	 * Resumes Calculation if Stateobject is passed
	 * @param algorithms
	 * @param safeTime
	 * @throws RemoteException
	 */
	void startCalculation(ArrayList<AlgorithmThread> algorithms, int safeTime) throws RemoteException;
	
	/**
	 * returns a list of problemSets currently not finished
	 * @return
	 */
	Object getAlgorithmStates() throws RemoteException;
	
	/**
	 * stops calculation
	 */
	void stopCalculation() throws RemoteException;
	
	//TODO create function to read stored highscores into board
	/**
	 * returns the best highscore until now
	 * @return
	 */
	Board getHighscore() throws RemoteException;
	
	/**
	 * returns a list of algorithms the server supports
	 * this can change due to plugin features
	 * @return
	 */
	ArrayList<String> getAvailableAlgorithms() throws RemoteException;
}
