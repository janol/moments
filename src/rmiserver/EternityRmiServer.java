package rmiserver;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

import plugin.AlgorithmListPlugin;
import plugin.HandAlgorithmList;

import exceptions.AlgorithmInstantiationException;
import exceptions.PlacingException;

import solver.*;
import threading.AlgorithmThread;

import basics.*;

/**
 * represents the rmi server
 * TODO add thread object to create backups
 * @author janolbrich
 *
 */
public class EternityRmiServer implements Server {
	
	private EternityThreadBasedSolver eternitySolver = null;
	private Thread eternityThread = null;
	private ServerCalculation calculationThread = null;
	private AlgorithmListPlugin eternityAlgorithmList;
	
	public EternityRmiServer() {
		this.eternityAlgorithmList = new HandAlgorithmList();
	}
	
	public Object getAlgorithmStates() {
		this.eternitySolver.pauseCalculation();
		Object state = this.eternitySolver.getAlgorithmStates();
		this.eternitySolver.resumeCalculation();
		
		return state;
	}

	public ArrayList<String> getAvailableAlgorithms() {
		return this.eternityAlgorithmList.getAlgorithmList();
	}

	@SuppressWarnings("unchecked")
	public Board getHighscore() {
		Board highscoreBoard = null;
		int highscore = 0;
		
		//select board of all threads
		ArrayList<AlgorithmThread> algos = (ArrayList<AlgorithmThread>) this.getAlgorithmStates();
		for (AlgorithmThread status: algos) {
			int tmpHighscore = status.getStateObject().getHighscore();
			
			if (highscore <= tmpHighscore) {
				highscore = tmpHighscore;
				highscoreBoard = status.getStateObject().getHighscoreBoard();
			}
		}
		return highscoreBoard;
	}

	public Board getSolution() {
		return this.eternitySolver.getSolution();
	}

	public RunningStates getStatus() {
		if (this.eternityThread == null) {
			return RunningStates.INACTIVE;
		}
		else if (this.eternityThread.getState() == Thread.State.TERMINATED) {
			return RunningStates.TERMINATED;
		}
		else {
			return RunningStates.RUNNING;
		}
	}

	public void startCalculation(String algorithmType, Tile[] tiles,
			Hint[] hints, int dimX, int dimY, int safeTime)
			throws RemoteException {
		
		//create solver object
		try {
			this.eternitySolver = new EternityThreadBasedSolver(algorithmType,
					new EternityBoard(dimX, dimY), tiles, hints);
		} catch (PlacingException e) {
			e.printStackTrace();
		} catch (AlgorithmInstantiationException e) {
			e.printStackTrace();
		}

		
		//create thread object for backups and start thread
		calculationThread = new ServerCalculation(this.eternitySolver, safeTime);
		this.eternityThread = new Thread(calculationThread);
		this.eternityThread.start();
	}

	public void stopCalculation() {
		this.calculationThread.stopCalculation();
		
		//wait to shut down the algorithms
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.eternityThread = null;
	}

	
	public static void main(String args[]) {
		
		try {
		    Server obj = new EternityRmiServer();
		    Server stub = (Server) UnicastRemoteObject.exportObject(obj, 0);

		    // Bind the remote object's stub in the registry
		    Registry registry = LocateRegistry.getRegistry();
		    registry.rebind("eternity", stub);

		    System.err.println("Server ready");
		} catch (Exception e) {
		    System.err.println("Server exception: " + e.toString());
		    e.printStackTrace();
		}
	    }

	public void startCalculation(ArrayList<AlgorithmThread> algorithms,
			int safeTime) throws RemoteException {
		//create solver object
		this.eternitySolver = new EternityThreadBasedSolver(algorithms);
		
		//create thread object for backups and start thread
		calculationThread = new ServerCalculation(this.eternitySolver, safeTime);
		this.eternityThread = new Thread(calculationThread);
		this.eternityThread.start();
	}
}
