package rmiserver.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import io.EternityTileReader;

import org.junit.*;

import exceptions.AlgorithmInstantiationException;
import exceptions.EmptyFileException;
import exceptions.NoFileException;
import exceptions.PlacingException;

import basics.*;

import rmiserver.*;
import solver.EternityThreadBasedSolver;


public class ServerCalculationTest {
	private ServerCalculation sc;
	private EternityThreadBasedSolver eternitySolver;
	private EternityTileReader eternityReader = null;
	private Tile[] eternityTiles;
	
	@Before
	public void setUp() throws IOException, EmptyFileException, PlacingException, AlgorithmInstantiationException, NoFileException {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
		
		this.eternityReader = new EternityTileReader();
		this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles10x10.txt");
		this.eternitySolver = new EternityThreadBasedSolver("algorithm.BacktrackAlgorithm", 
				new EternityBoard(4,4), this.eternityTiles, new EternityHint[0]);
		sc = new ServerCalculation(this.eternitySolver, 1);	
	}
	
	@Test
	public void testStartCalculation() {
		Thread run = new Thread(sc);
		run.start();
		
		assertEquals(Thread.State.RUNNABLE, run.getState());
		
		sc.stopCalculation();
	}
	
	@Test
	public void testStopCalculation() throws InterruptedException {
		Thread run = new Thread(sc);
		run.start();
		assertEquals(Thread.State.RUNNABLE, run.getState());
		
		sc.stopCalculation();
		
		//wait some time to make sure threads are shut down
		Thread.sleep(1000);
		
		assertEquals(Thread.State.TERMINATED, run.getState());
	}
	
	@Test
	public void testBackup() throws PlacingException, AlgorithmInstantiationException, IOException, EmptyFileException, InterruptedException {
		//make sure file does not exist
		File f = new File("tmp" + File.separator + "state.e2");
		
		if (f.exists()) {
			f.delete();
		}
		
		Thread run = new Thread(sc);
		run.start();
		
		
		//make sure file is created
		Thread.sleep(2000);
		
		assertTrue(f.exists());
		
		sc.stopCalculation();
	}
}
