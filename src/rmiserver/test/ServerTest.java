package rmiserver.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import io.EternityTileReader;
import io.StateSaver;

import org.junit.*;

import exceptions.EmptyFileException;
import exceptions.NoFileException;

import basics.*;

import rmiserver.*;
import threading.AlgorithmThread;

public class ServerTest {
	
	private EternityRmiServer ers;
	private String algorithmType = "algorithm.BacktrackAlgorithm";
	private EternityTileReader eternityReader = null;
	private Tile[] eternityTiles;
	private int safeTime = 600;
	
	@Before
	public void setUp() {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
		ers = new EternityRmiServer();
		this.eternityReader = new EternityTileReader();
	}

	@Test
	public void testGetStatusInactive() {
		assertEquals(ers.getStatus(), RunningStates.INACTIVE);
	}

	@Test
	public void testGetStatusRunning() throws IOException, EmptyFileException, NoFileException {
		this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles10x10.txt");
		ers.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 10, 10, safeTime);
		assertEquals(RunningStates.RUNNING, ers.getStatus());
		ers.stopCalculation();
	}

	@Test
	public void testGetStatusTerminated() throws InterruptedException, IOException, EmptyFileException, NoFileException {
		this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles4x4.txt");
		ers.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 4, 4, safeTime);
		do {
			Thread.sleep(10);
		} while (ers.getStatus() == RunningStates.RUNNING);
		
		assertEquals(RunningStates.TERMINATED, ers.getStatus());
	}

	@Test
	public void testGetSolution() throws InterruptedException, IOException, EmptyFileException, NoFileException {
		this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles4x4.txt");
		ers.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 4, 4, safeTime);
		
		do {
			Thread.sleep(10);
		} while (ers.getStatus() != RunningStates.TERMINATED);
		
		assertTrue(TestSolution.isSolution(ers.getSolution()));
	}

	@Test
	public void testGetAlgorithmStates() throws IOException, EmptyFileException, NoFileException {
		this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles10x10.txt");
		ers.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 10, 10, safeTime);
		
		assertTrue(ArrayList.class.isInstance(ers.getAlgorithmStates()));
		
		ers.stopCalculation();
	}

	@Test
	public void testStopCalculation() throws IOException, EmptyFileException, InterruptedException, NoFileException {
		this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles10x10.txt");
		ers.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 10, 10, safeTime);
		assertEquals(RunningStates.RUNNING, ers.getStatus());
		
		ers.stopCalculation();
		
		Thread.sleep(1000);
		
		assertEquals(RunningStates.INACTIVE, ers.getStatus());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testReturnCalculationState() throws RemoteException, InterruptedException {
		ArrayList<AlgorithmThread> tmpAlgo = (ArrayList<AlgorithmThread>) 
				StateSaver.getObject("resources" + File.separator + "state10x10.e2");
		ers.startCalculation(tmpAlgo, safeTime);
		do {
			Thread.sleep(1000);
		} while (ers.getStatus() != RunningStates.TERMINATED);
		
		assertTrue(TestSolution.isSolution(ers.getSolution()));
	}

	//test is comparing the solution with the highscore for now
	@Test
	public void testGetHighscore() throws IOException, EmptyFileException, InterruptedException, NoFileException {
		this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles10x10fast.txt");
		ers.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 10, 10, safeTime);
		
		do {
			Thread.sleep(1000);
		} while (ers.getStatus() != RunningStates.TERMINATED);
		
		assertTrue(ers.getSolution().equals(ers.getHighscore()));
	}

	//TODO implement algorithm plugin system
	@Test
	public void testGetAvailableAlgorithms() {
		ArrayList<String> algosGiven = new ArrayList<String>();
		algosGiven.add("algorithm.BacktrackAlgorithm");
		algosGiven.add("algorithm.SimpleBacktracker");
		
		assertTrue(algosGiven.equals(ers.getAvailableAlgorithms()));
	}

}
