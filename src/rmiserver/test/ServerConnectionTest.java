package rmiserver.test;

import static org.junit.Assert.*;
import io.EternityTileReader;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.junit.*;

import exceptions.EmptyFileException;
import exceptions.NoFileException;

import rmiserver.EternityRmiServer;
import rmiserver.RunningStates;
import rmiserver.Server;
import basics.EternityHint;
import basics.Log4jConfigurator;
import basics.Tile;

/**
 * connection test
 * rmiregistry has to be running
 * @author janolbrich
 *
 */
public class ServerConnectionTest {
	private Server stub;
	private String algorithmType = "algorithm.BacktrackAlgorithm";
	private EternityTileReader eternityReader = null;
	private Tile[] eternityTiles;
	private int safeTime = 600;
	String host =  null;
	
	@Before
	public void setUp() throws IOException, EmptyFileException, NoFileException {
		if(!Log4jConfigurator.isConfigured()) {
			Log4jConfigurator.configure();
		}
		
		//start server
		try {
		    Server obj = new EternityRmiServer();
		    Server stub = (Server) UnicastRemoteObject.exportObject(obj, 0);

		    // Bind the remote object's stub in the registry
		    Registry registry = LocateRegistry.getRegistry();
		    registry.rebind("eternity", stub);

		    System.err.println("Server ready");
		} catch (Exception e) {
		    System.err.println("Server exception: " + e.toString());
		    e.printStackTrace();
		}
	
		//connect client
		try {
		    Registry registry = LocateRegistry.getRegistry(host);
		    stub = (Server) registry.lookup("eternity");
		} catch (Exception e) {
		    System.err.println("Client exception: " + e.toString());
		    e.printStackTrace();
		}
		
		this.eternityReader = new EternityTileReader();
		this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles10x10.txt");
	}
	
	@Test
	public void testStartCalculation() throws RemoteException {
		stub.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 10, 10, safeTime);
		assertEquals(RunningStates.RUNNING, stub.getStatus());
		
		stub.stopCalculation();
	}
	
	@Test
	public void testStopCalculation() throws RemoteException {
		stub.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 10, 10, safeTime);
		
		stub.stopCalculation();
		assertEquals(RunningStates.INACTIVE, stub.getStatus());
	}
	
	@Test
	public void testNewConnection() throws RemoteException {
		stub.startCalculation(algorithmType, this.eternityTiles, new EternityHint[0], 10, 10, safeTime);
		assertEquals(RunningStates.RUNNING, stub.getStatus());
		
		stub = null;
		
		try {
		    Registry registry = LocateRegistry.getRegistry(host);
		    stub = (Server) registry.lookup("eternity");
		} catch (Exception e) {
		    System.err.println("Client exception: " + e.toString());
		    e.printStackTrace();
		}
		
		assertEquals(RunningStates.RUNNING, stub.getStatus());
		
		stub.stopCalculation();
	}
}
