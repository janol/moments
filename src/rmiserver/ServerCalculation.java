package rmiserver;

import io.StateSaver;

import java.io.*;

import solver.*;

public class ServerCalculation implements Runnable {
	
	private final int SLICE_TIME = 100;
	
	private EternityThreadBasedSolver eternitySolver = null;
	private int SAFE_TIME = 1000*600;
	
	private boolean userAbort;
	
	public ServerCalculation(EternityThreadBasedSolver solver, int safeTime) {
		this.eternitySolver = solver;
		this.SAFE_TIME = safeTime*1000;
		this.userAbort = false;
	}
	
	public void run() {
		Thread eternityThread = new Thread(this.eternitySolver);
		eternityThread.start();
		
		long startTime = System.currentTimeMillis();
		do {
			// wait slicetime 
			try {
				Thread.sleep(SLICE_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// save clone
			if (System.currentTimeMillis() > (startTime + SAFE_TIME)){
				eternitySolver.pauseCalculation();
				Object state = eternitySolver.getAlgorithmStates();
				eternitySolver.resumeCalculation();
				StateSaver.saveObject(state, "tmp" + File.separator + "state.e2");
				startTime = System.currentTimeMillis();
			}
		} while ((!userAbort) && (eternityThread.getState() != Thread.State.TERMINATED));
		
		this.eternitySolver.stopThreads();
	}

	public void stopCalculation() {
		this.userAbort = true;
	}
}
