package threading;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;

import exceptions.AlgorithmInstantiationException;
import exceptions.PlacingException;

import algorithm.Algorithm;
import algorithm.AlgorithmState;
import algorithm.EternityAlgorithmState;
import basics.*;

public class EternityAlgorithmThread extends AlgorithmThread {
	private static final long serialVersionUID = 8230689876320841975L;
	private Algorithm eternityAlgorithm;
	private Board solution = null;
	
	private AlgorithmState state;
	
	private boolean threadPause = false;
	private boolean threadRun = true;
	
	
	private static final Logger LOGGER =
		Logger.getLogger(EternityAlgorithmThread.class);
	
	public EternityAlgorithmThread(String algoType, Board board, Tile[] tiles,
			Hint[] hints) throws PlacingException,
			AlgorithmInstantiationException {
		Class<?> algoClazz;
		try {
			algoClazz = Class.forName(algoType);
		} catch (ClassNotFoundException e2) {
			throw new AlgorithmInstantiationException(e2);
		}
		
		Constructor<?> constructor;
		try {
			constructor = algoClazz.getConstructor(
					basics.Board.class, basics.Tile[].class,
					basics.Hint[].class);
		} catch (SecurityException e1) {
			throw new AlgorithmInstantiationException(e1);
		} catch (NoSuchMethodException e1) {
			throw new AlgorithmInstantiationException(e1);
		}
		
		try {
			this.eternityAlgorithm = (Algorithm) constructor.newInstance(board,
					tiles, hints);
		} catch (IllegalArgumentException e) {
			throw new AlgorithmInstantiationException(e);
		} catch (InstantiationException e) {
			throw new AlgorithmInstantiationException(e);
		} catch (IllegalAccessException e) {
			throw new AlgorithmInstantiationException(e);
		} catch (InvocationTargetException e) {
			throw new AlgorithmInstantiationException(e);
		}
		
		this.state = new EternityAlgorithmState(eternityAlgorithm);
	}

	@Override
	public final void pause() {
		this.threadPause=true;
		
		LOGGER.info("Trying to pause this thread");
	}
	
	@Override
	public final void resumeFromePause() {
		LOGGER.info("Trying to wake this thread");
		synchronized (this){
			this.wakeUpTime = System.currentTimeMillis();
			this.threadPause = false;
//			notify();
			notifyAll();
		}
	}
	
	public final void run(){
		LOGGER.info("Starting thread");
		
		wakeUpTime = System.currentTimeMillis();
		
		boolean done = false;
		this.threadRun = true;
		state.startCalculation();
		
		do{
			//calculate the board step by step

			done = this.eternityAlgorithm.calculateStep();
			
			state.checkDepth(this.eternityAlgorithm.getDepth());
			
			if(state.getStepCount() % 1E6 == 0) {
				LOGGER.info("NumSteps: " + state.getStepCount()/1E6);
				LOGGER.info("Time: " + state.getCalculationTime());
				LOGGER.info("RecentHighscore: " + state.getRecentHighscore());
				LOGGER.info("Recent Backtrack: " + state.getRecentBacktrack());
				LOGGER.info("Highscore" + state.getHighscore());
				LOGGER.info("NextTime" + state.getTimeToNextHighscore() + "\n");
			}
			
			synchronized (this) {
				//we'll have to pause the thread to be able to synchronize
				while (this.threadPause) {
					try {
						wait();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} 
			//do as long we don't stop the thread or find a solution
		} while(done && this.threadRun && !this.eternityAlgorithm.isSolved());
		
		//in case we have a solution save it
		if (this.eternityAlgorithm.isSolved()){
			this.solution = this.eternityAlgorithm.getBoard();
		}
	}

	@Override
	public void stopThread() {
		this.threadRun = false;
	}

	@Override
	public Algorithm getAlgorithmObject() {
		return this.eternityAlgorithm;
	}

	@Override
	public Board getSolution() {
		return this.solution;
	}

	@Override
	public AlgorithmState getStateObject() {
		return this.state;
	}
}
