package threading;

import java.io.*;

import basics.Board;

import algorithm.Algorithm;
import algorithm.AlgorithmState;

//import org.apache.log4j.Logger;

public abstract class AlgorithmThread extends Thread implements Serializable{
	private static final long serialVersionUID = -4418035330913148656L;
	
//	private static final Logger LOGGER = Logger.getLogger(AlgorithmThread.class);
	
	protected long wakeUpTime;
	
//	public AlgorithmThread() {
//		wakeUpTime = System.currentTimeMillis();
//	}
	
	/**
	 * pauses the thread to be able to save the object
	 */
	public abstract void pause();
	
	/**
	 * resumes the thread after being paused
	 */
	public abstract void resumeFromePause();
	
	/**
	 * terminates the thread
	 */
	public abstract void stopThread();
	
	/**
	 * returns the AlgorithmObject used for calculation
	 * it's the actual state right now
	 * @return
	 */
	public abstract Algorithm getAlgorithmObject();
	
	/**
	 * returns full Board
	 * @return
	 */
	public abstract Board getSolution();
	
	/**
	 * Return the time at which this thread was waked
	 * @return
	 */
	public long getWakeUpTime() {
		return this.wakeUpTime;
	}	
	
	/**
	 * returns the algorithmState object of this thread
	 * @return
	 */
	public abstract AlgorithmState getStateObject();
}
