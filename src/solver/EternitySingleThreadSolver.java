package solver;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;

import algorithm.Algorithm;
import basics.*;


import exceptions.AlgorithmInstantiationException;
import exceptions.PlacingException;

/**
 * Single Thread Solver simply starts the calculation
 * needs enum to distinguish algorithms
 * @author OLJA100
 *
 */
public class EternitySingleThreadSolver extends Solver {
	private Algorithm algo;
	private static final Logger LOGGER =
		Logger.getLogger(EternitySingleThreadSolver.class);

	public EternitySingleThreadSolver(String algoType, Board board, Tile[] tiles,
			Hint[] hints) throws PlacingException,
			AlgorithmInstantiationException {
		Class<?> algoClazz;
		try {
			algoClazz = Class.forName(algoType);
		} catch (ClassNotFoundException e2) {
			throw new AlgorithmInstantiationException(e2);
		}
		
		Constructor<?> constructor;
		try {
			constructor = algoClazz.getConstructor(
					basics.Board.class, basics.Tile[].class,
					basics.Hint[].class);
		} catch (SecurityException e1) {
			throw new AlgorithmInstantiationException(e1);
		} catch (NoSuchMethodException e1) {
			throw new AlgorithmInstantiationException(e1);
		}
		
		try {
			this.algo = (Algorithm) constructor.newInstance(board, tiles, hints);
		} catch (IllegalArgumentException e) {
			throw new AlgorithmInstantiationException(e);
		} catch (InstantiationException e) {
			throw new AlgorithmInstantiationException(e);
		} catch (IllegalAccessException e) {
			throw new AlgorithmInstantiationException(e);
		} catch (InvocationTargetException e) {
			throw new AlgorithmInstantiationException(e);
		}
	}
	
	@Override
	public Board calculateSolution() {
		boolean done = true;
		long time =  -System.currentTimeMillis();
		do{
				done = algo.calculateStep();
		} while(done && !algo.isSolved());
		time+=System.currentTimeMillis();
		LOGGER.info("Time needed to solve the Puzzle: " + time + "ms");
		return algo.getBoard();
	}

}
