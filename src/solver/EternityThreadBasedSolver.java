package solver;

import threading.AlgorithmThread;
import threading.EternityAlgorithmThread;

import java.lang.Thread.State;
import java.util.*;

import org.apache.log4j.*;

import exceptions.AlgorithmInstantiationException;
import exceptions.PlacingException;

import basics.*;

public class EternityThreadBasedSolver extends ThreadBasedSolver implements Runnable{
	private static final Logger LOGGER =
		Logger.getLogger(EternityThreadBasedSolver.class);

	private ArrayList<AlgorithmThread> eternityAlgorithms;
	private Board solution = null;
	
	private static final int MAX_CONCURRENT_THREADS =
		Runtime.getRuntime().availableProcessors();
	
	final long SLICE_TIME = 100;
	final long THREAD_TIME = 10000;
	
	private boolean pause = false;

	public EternityThreadBasedSolver(String algoType, Board board, Tile[] tiles,
			Hint[] hints) throws PlacingException,
			AlgorithmInstantiationException {
		this.eternityAlgorithms = getProblemDefinitions(algoType, board, tiles,
				hints);
	}

	// Use when already some Objects exist
	public EternityThreadBasedSolver(ArrayList<AlgorithmThread> algos) {
		this.eternityAlgorithms = algos;
	}

	@Override
	public Board calculateSolution() {
		ArrayList<AlgorithmThread> runningThreads =
			new ArrayList<AlgorithmThread>();
		ArrayList<AlgorithmThread> waitingThreads =
			new ArrayList<AlgorithmThread>();
		ArrayList<AlgorithmThread> threadsToBeUnqueued =
			new ArrayList<AlgorithmThread>();
		
		LOGGER.info("Starting calculation with max " + MAX_CONCURRENT_THREADS
				+ " cores available");
		
		waitingThreads.addAll(this.eternityAlgorithms);

		try {
			// iterate until either a solution is found or all threads have finished
			// unsuccessfully
			endless : while(runningThreads.size()+waitingThreads.size() > 0) {
				threadsToBeUnqueued.clear();

				// check all threads currently marked as running
				for(AlgorithmThread thread : runningThreads) {
					if(thread.getState() == State.TERMINATED) {
						if(thread.getAlgorithmObject().isSolved()) {
							// calculation finished successfully
							this.solution = thread.getSolution();
							break endless;
						} else {
							// calculation of this thread finished unsuccessfully
							// unqueue it
							threadsToBeUnqueued.add(thread);
						} 
					} else if ((System.currentTimeMillis()
							> thread.getWakeUpTime() + THREAD_TIME) || (pause)) {
						// thread has exceeded its time-slice
						// unqueue it
						// TODO: check whether exception handling must be executed here
						threadsToBeUnqueued.add(thread);
						
						// send the thread to waiting if it was running
//						if(thread.getState() == State.RUNNABLE) {
							LOGGER.info("Trying to pause the thread " + thread.getId());
							thread.pause();
//						}
					}
				}

				// unqueue previously running threads
				for(AlgorithmThread thread : threadsToBeUnqueued) {
					runningThreads.remove(thread);
					
					if(thread.getState() != State.TERMINATED) {
						// thread has not finished its calculation so add it to the
						// list of waiting threads
						waitingThreads.add(thread);
					}
				}				
				threadsToBeUnqueued.clear();

				// wake up to MAX_CONCURRENT_THREADS threads
				int numThreadsToWake = MAX_CONCURRENT_THREADS - runningThreads.size();
				for(AlgorithmThread thread : waitingThreads) {
					if(!(numThreadsToWake > 0) || (pause)) {
						break;
					}
					LOGGER.info("Encountered thread status of "
							+ thread.getState());
					if(thread.getState() == State.NEW) {
						// run freshly created thread...
						thread.start();
					} else if(thread.getState() == State.WAITING) {
						// wake the thread...
						thread.resumeFromePause();
					} else if(thread.getState() == State.BLOCKED) {
						LOGGER.info("Ignoring thread");
						continue;
					} else {
						LOGGER.error("Bailing out");
						throw new IllegalMonitorStateException("Encountered thread in "
								+ "waiting pool which was neither new nor waiting");
					}
					// ...and add it to the list of running threads
					runningThreads.add(thread);
					threadsToBeUnqueued.add(thread);
					numThreadsToWake--;
				}
				
				if((numThreadsToWake > 0) && !(pause) && (waitingThreads.size() > 0)) {
					LOGGER.info("Failed to wake " + numThreadsToWake + " with "
							+ waitingThreads.size() + " threads waiting");
				}
				
				// unqueue previously waiting threads
				waitingThreads.removeAll(threadsToBeUnqueued);
				
				Thread.sleep(SLICE_TIME);
			}
		} catch (InterruptedException e) {
			// unrecoverable as per current design
			e.printStackTrace();
		}

		// explicitly shut down any threads still running
		for(AlgorithmThread thread : runningThreads) {
			thread.stopThread();
		}
		
		return this.solution;

	}
	
	// TODO hat to clone the objects and pause the threads
	@Override
	public ArrayList<AlgorithmThread> getAlgorithmObject() {
		return this.eternityAlgorithms;
	}

	/**
	 * Generate the algorithm-threads along with their respective set
	 * of hints
	 * @param algoType Type of algorithm to use
	 * @param board the basic board
	 * @param tiles the list of tiles to be placed
	 * @param hints the predetermined hints
	 * @return
	 * @throws PlacingException 
	 * @throws AlgorithmInstantiationException 
	 */
	private ArrayList<AlgorithmThread> getProblemDefinitions(String algoType,
			Board board, Tile[] tiles, Hint[] hints) throws PlacingException, AlgorithmInstantiationException {
		
		// generate the sets of tile-corner-matchings
		ArrayList<Hint[]> problemSets = HintGenerator.generateProblemSets(
				tiles, hints, board.getDimensionX(), board.getDimensionY());
		
		// generate complete sets of hints and one corresponding algorithm thread
		ArrayList<AlgorithmThread> algos = new ArrayList<AlgorithmThread>();
		for (Hint[] cornerHints: problemSets) {
			Hint[] completeHintSet = new Hint[cornerHints.length + hints.length];
			
			// copy the pre-determined set of hints
			for (int i = 0; i < hints.length; i++){
				completeHintSet[i] = hints[i];
			}
			
			// generated corner hints
			for (int i = 0; i <cornerHints.length; i++){
				completeHintSet[i + hints.length] = cornerHints[i];
			}
			
			algos.add(new EternityAlgorithmThread(algoType, board, tiles,
					completeHintSet));
		}

		return algos;
	}
	
	/**
	 * starts calculateSolution as thread to be able to abort calculation
	 * @return
	 */
	public final void run() {
		this.calculateSolution();
	}
	
	/**
	 * returns solution of board
	 * @return
	 */
	public Board getSolution() {
		return this.solution;
	}
	
	/**
	 * pauses calculation
	 * sets all running threads asleep and does not start new ones
	 */
	public void pauseCalculation() {
		pause = true;
	}
	
	/**
	 * resume from pause
	 */
	public void resumeCalculation() {
		pause = false;
	}
	
	/**
	 * returns a clone of all algorithms currently not finished
	 * @return
	 */
	public Object getAlgorithmStates() {
		Object clone = null;
		try {
			//make sure to wait till all algorithms are asleep
			Thread.sleep(2*SLICE_TIME);
			
			 clone = CloneObject.deepCopy(eternityAlgorithms);
		} catch (Exception e) {
			System.out.println("deep copy");
			e.printStackTrace();
		}
		return clone;
	}
	
	/**
	 * stop all running threads
	 */
	public void stopThreads() {
		pause = true;
		try {
			Thread.sleep(2*SLICE_TIME);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// explicitly shut down any threads still running
		for(AlgorithmThread thread : this.eternityAlgorithms) {
			thread.stopThread();
		}
	}
}
