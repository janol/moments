package solver;

import basics.Board;

public abstract class Solver {
	
	
	/**
	 * Starts the threads to solve the board
	 * Calculates the Board and returns fully solved
	 * @return
	 */
	public abstract Board calculateSolution();
}
