package solver.test;

import static org.junit.Assert.*;

import io.EternityTileReader;

import java.io.File;
import java.util.*;

import org.junit.*;

import threading.AlgorithmThread;
import threading.EternityAlgorithmThread;
import solver.EternityThreadBasedSolver;
import solver.ThreadBasedSolver;

import exceptions.AlgorithmCountException;

import basics.*;

public class EternitySolverTest {
	private ThreadBasedSolver ESolver;
	private Tile[] eternityTiles = null;
	private Hint[] eternityHints = null;
	private EternityTileReader eternityReader;
	
	@Before
	public void setUp() throws Exception {
		this.eternityReader = new EternityTileReader();
		
		this.eternityTiles = this.eternityReader.getTiles("resources" + File.separator + "tiles4x4.txt");
		this.eternityHints = this.eternityReader.getHints("resources" + File.separator + "hints4x4.txt");
		
		this.ESolver = new EternityThreadBasedSolver("algorithm.BacktrackAlgorithm", new EternityBoard(4,4),
				this.eternityTiles, this.eternityHints);
	}

	@Test 
	public void testGetAlgorithmObject() throws AlgorithmCountException{
		ArrayList<AlgorithmThread> test = null;
		test = this.ESolver.getAlgorithmObject();
		
		assertTrue(test.get(0) instanceof EternityAlgorithmThread);
	}
	
	@Test
	public void testCalculateSolution() {
		fail("Not yet implemented");
	}

	@Test
	public void testPause() {
		fail("Test Not yet implemented but code");
	}

	@Test
	public void testResume() {
		fail("Test Not yet implemented but code");
	}
	
	@Test
	public void testCopy() {
		fail("Test Not yet implemented but code");
	}

}
