package solver;

import java.util.*;

import threading.AlgorithmThread;

import basics.Board;

public abstract class ThreadBasedSolver extends Solver {

	@Override
	public abstract Board calculateSolution();

	/**
	 * returns Solving Object (form thread no count) with all data for saving
	 * @param count
	 * @return
	 */
	public abstract ArrayList<AlgorithmThread> getAlgorithmObject();

}
